import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/src.dart';

import '../configs/configs.dart';

enum AccountType { Email, Phone }

class AppValid {
  AppValid._();

  static validateFullName(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) return AppLocalizations.of(context).translate("valid_full_name");
      return null;
    };
  }

  static validateEmail(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return AppLocalizations.of(context).translate("valid_enter_email");
      } else {
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);

        if (!regex.hasMatch(value)) return AppLocalizations.of(context).translate("valid_email");
        return null;
      }
    };
  }

  static isLoginWithEmailOrPhoneNumber(String value) {
    Pattern pattern = r'.*[a-zA-Z]+.*';
    RegExp regex = new RegExp(pattern);

    if (!regex.hasMatch(value)) return AccountType.Phone;
    return AccountType.Email;
  }

  static validateEmailOrPhoneNumber(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Trường này không được để trống";
      } else {
        Pattern pattern;
        RegExp regex;

        switch (isLoginWithEmailOrPhoneNumber(value)) {
          case AccountType.Phone:
            pattern = r'^(?:[+0]9)?[0-9]{10}$';
            regex = new RegExp(pattern);

            if (value.length < 10) return "Số điện thoại phải ít nhất 10 số";
            if (!regex.hasMatch(value)) return "Số điện thoại bạn nhập vào không đúng định dạng";
            return null;
          case AccountType.Email:
            pattern =
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
            regex = new RegExp(pattern);

            if (!regex.hasMatch(value)) return "Email bạn nhập vào không đúng đinh dạng";
            return null;
        }
      }
    };
  }

  static validatePassword(BuildContext context) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Trường này không được để trống";
      } else if (value.length < 8) {
        return "Mật khẩu phải chứa ít nhất 8 ký tự";
      } else {
        Pattern pattern = r'^[0-9a-zA-Z!@#\$&*~]{6,}$';
        RegExp regex = new RegExp(pattern);

        if (!regex.hasMatch(value)) return AppLocalizations.of(context).translate("valid_password");
        return null;
      }
    };
  }

  static validateConfirmPassword(BuildContext context, String password) {
    return (value) {
      if (value == null || value.length == 0) {
        return "Trường này không được để trống";
      } else if (value.length < 8) {
        return "Mật khẩu phải chứa ít nhất 8 ký tự";
      } else {
        Pattern pattern = r'^[0-9a-zA-Z!@#\$&*~]{6,}$';
        RegExp regex = new RegExp(pattern);

        if (!regex.hasMatch(value)) {
          return "Mật khẩu bạn nhập vào không đúng định dạng";
        } else {
          if (value == password) return null;
          return "Mật khẩu bạn nhập vào không đúng với mật khẩu phía trên";
        }
      }
    };
  }

  static validatePhoneNumber(BuildContext context) {
    Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
    RegExp regex = new RegExp(pattern);

    return (value) {
      if (value == null || value.length == 0) {
        return AppLocalizations.of(context).translate("valid_enter_phone");
      } else if (value.length != 10) {
        return AppLocalizations.of(context).translate("valid_phone");
      } else if (!regex.hasMatch(value)) {
        return AppLocalizations.of(context).translate("valid_phone");
      }

      return null;
    };
  }
}
