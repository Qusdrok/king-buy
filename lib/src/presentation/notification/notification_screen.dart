import 'package:flutter/material.dart';

import '../../src.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  NotificationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotificationViewModel>(
      viewModel: NotificationViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Thông báo",
                  alignTitle: Alignment.centerLeft,
                  trans: false,
                ),
              ],
            ),
          ),
        );
      },
    );
  }


  Widget _buildNotification({NotificationModel model}) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routers.notification_detail,
        arguments: model,
      ),
      child: Container(
        width: AppSize.screenWidth,
        height: 110,
        padding: EdgeInsets.only(top: 10, bottom: 10, left: 12, right: 12),
        color: Colors.white,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    model.title,
                    style: AppStyles.DEFAULT_MEDIUM_BOLD,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5),
                  Expanded(
                    child: Text(
                      model.content,
                      style: AppStyles.DEFAULT_MEDIUM,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(height: 5),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
