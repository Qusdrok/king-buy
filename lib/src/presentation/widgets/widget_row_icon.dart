import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetRowIcon extends StatelessWidget {
  final Function onTap;
  final String imgUrl;
  final String title;
  final double height;
  final double width;
  final Widget icon;
  final Color colorImg;

  WidgetRowIcon({
    this.onTap,
    this.imgUrl,
    this.title,
    this.height,
    this.width,
    this.icon,
    this.colorImg,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Row(
        children: [
          Image.asset(
            imgUrl,
            width: width ?? 25,
            height: height ?? 25,
            color: colorImg,
            fit: BoxFit.fill,
          ),
          const SizedBox(width: 12),
          Expanded(
            child: Text(
              title,
              style: AppStyles.DEFAULT_LARGE,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          icon ??
              Image.asset(
                AppImages.icChevron,
                width: 8,
                height: 10,
                fit: BoxFit.fill,
              ),
        ],
      ),
    );
  }
}
