import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetLogo extends StatelessWidget {
  final double width;
  final double height;

  WidgetLogo({this.width, this.height});

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.icLogo,
      width: width ?? AppSize.screenWidth / 3,
      height: height ?? AppSize.screenWidth / 3 + 10,
      fit: BoxFit.fill,
    );
  }
}
