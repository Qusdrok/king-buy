import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetSale extends StatelessWidget {
  final int sale;

  WidgetSale({@required this.sale});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 30,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: AppColors.red,
      ),
      alignment: Alignment.center,
      child: Text(
        "-${sale}%",
        style: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: Colors.white,
        ),
      ),
    );
  }
}
