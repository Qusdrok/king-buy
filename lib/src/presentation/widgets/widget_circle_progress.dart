import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetCircleProgress extends StatelessWidget {
  final Color color;
  final Color colorBackground;
  final Alignment alignment;
  final double size;
  final double strokeWidth;

  WidgetCircleProgress({
    this.color,
    this.size,
    this.strokeWidth,
    this.colorBackground,
    this.alignment,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size ?? 40,
      height: size ?? 40,
      padding: const EdgeInsets.all(5),
      alignment: alignment ?? Alignment.center,
      child: CircularProgressIndicator(
        strokeWidth: strokeWidth ?? 1.5,
        valueColor: AlwaysStoppedAnimation(color ?? AppColors.primary),
        backgroundColor: colorBackground ?? Colors.transparent,
      ),
    );
  }
}
