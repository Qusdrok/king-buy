import 'package:flutter/material.dart';

class WidgetFilledHalf extends StatelessWidget {
  final IconData icon;
  final double size;
  final Color color;

  WidgetFilledHalf({@required this.icon, this.size, this.color});

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      blendMode: BlendMode.srcATop,
      shaderCallback: (Rect rect) {
        return LinearGradient(
          stops: [0, 0.5, 0.5],
          colors: [color, color, color.withOpacity(0)],
        ).createShader(rect);
      },
      child: SizedBox(
        width: size,
        height: size,
        child: Icon(
          icon,
          size: size,
          color: Colors.grey[300],
        ),
      ),
    );
  }
}
