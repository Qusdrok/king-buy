import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetProduct extends StatefulWidget {
  final ProductModel productModel;
  final double width;

  WidgetProduct({
    this.productModel,
    this.width,
  });

  @override
  _WidgetProductState createState() => _WidgetProductState();
}

class _WidgetProductState extends State<WidgetProduct> {
  final AuthRepository _authRepository = AuthRepository();

  bool _hasGifts = false;
  bool _hasInstallment = false;
  int _sale, _star;

  @override
  void initState() {
    super.initState();
    _sale = widget.productModel?.sale_off ?? 0;
    _star = (widget.productModel?.star ?? 5).toInt();

    _hasGifts = widget.productModel?.gifts?.isNotEmpty ?? false;
    _hasInstallment = widget.productModel?.is_installment == 1 ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        RatingModel rating = await getProductRating();
        List<ReviewModel> reviews = await getProductReviews();
        DataModel data = await getProductDetail();

        data.rating = rating;
        data.reviews = reviews;
        Navigator.pushNamed(context, Routers.detail_product, arguments: data);
      },
      child: PhysicalModel(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        color: AppColors.grey.withOpacity(0.5),
        elevation: 1,
        child: Container(
          width: widget.width ?? 240,
          height: 455,
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            border: Border.all(
              width: 0.15,
              color: AppColors.grey,
            ),
            color: Colors.white,
          ),
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  WidgetImageNetwork(
                    url: widget.productModel != null
                        ? AppUtils.convertUrl(widget.productModel.image_source)
                        : "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
                    height: 240,
                    width: widget.width ?? 240,
                    fit: BoxFit.fill,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    widget.productModel?.name ?? "Product",
                    style: AppStyles.DEFAULT_LARGE,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 5),
                  Text(
                    widget.productModel?.brand_name ?? "OTO",
                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: AppColors.blue,
                    ),
                  ),
                  const SizedBox(height: 5),
                  WidgetRowStar(star: _star.toDouble()),
                  const SizedBox(height: 5),
                  Row(
                    children: [
                      Expanded(
                        child: WidgetTextMoney(
                          align: MainAxisAlignment.start,
                          money: widget.productModel?.sale_price ?? 600000,
                          textStyle: AppStyles.DEFAULT_LARGE.copyWith(
                            color: AppColors.red,
                          ),
                        ),
                      ),
                      Expanded(
                        child: WidgetTextMoney(
                          money: widget.productModel?.price ?? 600000,
                          textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  if (_hasGifts)
                    Row(
                      children: [
                        Image.asset(
                          AppImages.icGiftP2,
                          width: 20,
                          height: 20,
                          fit: BoxFit.fill,
                        ),
                        const SizedBox(width: 5),
                        Expanded(
                          child: Text(
                            widget.productModel.gifts[0].name,
                            style: AppStyles.DEFAULT_MEDIUM.copyWith(
                              color: AppColors.blue,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                  const SizedBox(height: 8),
                  _hasInstallment
                      ? Text(
                          "Trả góp 0%",
                          style: AppStyles.DEFAULT_SMALL_BOLD,
                        )
                      : SizedBox(height: 5),
                ],
              ),
              if (_sale > 0)
                Positioned(
                  top: 0,
                  right: 0,
                  child: WidgetSale(sale: _sale),
                ),
              const SizedBox(height: 8),
            ],
          ),
        ),
      ),
    );
  }

  Future<DataModel> getProductDetail() async {
    NetworkState state = await _authRepository.getProductDetail(widget.productModel.id);
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }

  Future<List<ReviewModel>> getProductReviews() async {
    NetworkState state = await _authRepository.getProductReviews(widget.productModel.id);
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }

  Future<RatingModel> getProductRating() async {
    NetworkState state = await _authRepository.getProductRatings(widget.productModel.id);
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }
}
