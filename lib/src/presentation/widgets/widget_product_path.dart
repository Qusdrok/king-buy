import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetProductPath extends StatelessWidget {
  final dynamic model;

  WidgetProductPath({@required this.model});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: AppSize.screenWidth,
      padding: const EdgeInsets.only(left: 15, top: 15),
      child: Row(
        children: [
          Image.asset(
            AppImages.icHomeBlack,
            width: 18,
            height: 15,
            fit: BoxFit.fill,
          ),
          const SizedBox(width: 5),
          _buildTile(title: "Danh mục"),
          const SizedBox(width: 5),
          Expanded(
            child: Row(
              children: model is CategoryModel
                  ? [
                      _buildTile(title: model.name),
                    ]
                  : [
                      _buildTile(title: model.product.category.parent.name),
                      const SizedBox(width: 5),
                      _buildTile(title: model.product.category.name),
                      const SizedBox(width: 5),
                      Expanded(
                        child: Row(
                          children: [
                            _buildArrow(),
                            const SizedBox(width: 5),
                            Expanded(
                              child: Text(
                                model.product.name,
                                style: AppStyles.DEFAULT_MEDIUM,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTile({String title}) {
    return Row(
      children: [
        _buildArrow(),
        const SizedBox(width: 5),
        Text(
          title,
          style: AppStyles.DEFAULT_MEDIUM,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  Widget _buildArrow() {
    return Image.asset(
      AppImages.icChevron,
      width: 6,
      height: 10,
      fit: BoxFit.fill,
    );
  }
}
