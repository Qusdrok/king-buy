import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetButton extends StatelessWidget {
  final String title;
  final TextStyle styleTitle;
  final double circular;
  final double height;
  final double width;
  final Function onTap;
  final Color colorBorder;
  final Color colorBg;

  WidgetButton({
    @required this.title,
    this.styleTitle,
    this.circular,
    this.height,
    this.width,
    this.onTap,
    this.colorBorder,
    @required this.colorBg,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        width: width ?? AppSize.screenWidth,
        height: height ?? 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(circular ?? 8)),
          border: Border.all(
            color: colorBorder ?? AppColors.red,
            width: 1,
          ),
          color: colorBg,
        ),
        alignment: Alignment.center,
        child: Text(
          title,
          style: styleTitle ??
              AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.red,
              ),
        ),
      ),
    );
  }
}
