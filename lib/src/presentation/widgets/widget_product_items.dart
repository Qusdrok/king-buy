import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetProductItems extends StatefulWidget {
  final String title;
  final String api;
  final int productId;
  final int brandId;
  final String searchWord;
  final double width;
  final Axis axis;
  final Widget loadMore;
  final EdgeInsets padding;

  WidgetProductItems({
    this.title,
    @required this.api,
    this.productId,
    this.width,
    this.axis,
    this.padding,
    this.loadMore,
    this.brandId,
    this.searchWord,
  });

  @override
  _WidgetProductItemsState createState() => _WidgetProductItemsState();
}

class _WidgetProductItemsState extends State<WidgetProductItems> {
  final AuthRepository authRepository = AuthRepository();

  @override
  Widget build(BuildContext context) {
    return WidgetLoadMore(
      itemBuilder: _buildProducts,
      dataRequester: _dataProducts,
      initRequester: _initProducts,
      autoLoadMore: true,
      limit: 4,
      isSingleWrap: true,
      axis: widget.axis,
      loadMore: widget.loadMore,
      padding: widget.padding,
    );
  }

  Widget _buildProducts(List data, BuildContext context, int index) {
    return Padding(
      padding: EdgeInsets.only(left: 5, right: 5, bottom: 15),
      child: WidgetProduct(
        productModel: data[index],
        width: widget.width,
      ),
    );
  }

  Future<List<ProductModel>> _initProducts(int limit) async {
    return await _getProducts(
      title: widget.title,
      api: widget.api,
      productId: widget.productId,
      brandId: widget.brandId,
      searchWord: widget.searchWord,
      limit: limit,
      offset: 0,
    );
  }

  Future<List<ProductModel>> _dataProducts(int limit, int offset) async {
    return await _getProducts(
      title: widget.title,
      api: widget.api,
      productId: widget.productId,
      searchWord: widget.searchWord,
      brandId: widget.brandId,
      offset: offset,
      limit: limit,
    );
  }

  Future<List<ProductModel>> _getProducts({
    String api,
    String title,
    int brandId,
    int productId,
    String searchWord,
    int limit = 5,
    int offset = 0,
  }) async {
    List<ProductModel> data = [];
    NetworkState state = await authRepository.getProducts(
      api: api,
      productId: productId,
      searchWord: searchWord,
      brandId: brandId,
      title: title,
      limit: limit,
      offset: offset,
    );

    if (state.isSuccess && state.data != null) data = state.data;
    return data;
  }
}
