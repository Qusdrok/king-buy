import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTextField extends StatelessWidget {
  final String hintText;
  final TextStyle hintStyle;
  final bool obscureText;
  final bool readOnly;
  final String suffixImg;
  final String prefixImg;
  final EdgeInsets paddingImg;
  final FormFieldValidator<String> validator;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final double height;
  final double circular;
  final Color colorBorder;
  final Function onTap;
  final ValueChanged<String> onSubmitted;

  WidgetTextField({
    this.hintText,
    this.circular,
    this.height,
    this.colorBorder,
    @required this.controller,
    this.suffixImg,
    this.prefixImg,
    this.readOnly = false,
    this.hintStyle,
    this.obscureText = false,
    this.onSubmitted,
    this.paddingImg,
    this.keyboardType = TextInputType.text,
    this.validator,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    var _borderTextField = _buildBorderTextField(
      color: colorBorder ?? AppColors.grey,
      circular: circular,
    );

    return TextFormField(
      onTap: onTap ?? () {},
      controller: controller,
      readOnly: readOnly,
      obscureText: obscureText,
      onFieldSubmitted: onSubmitted,
      validator: validator,
      keyboardType: keyboardType ?? TextInputType.text,
      decoration: InputDecoration(
        suffixIcon: suffixImg != null
            ? Padding(
                padding: paddingImg ?? EdgeInsets.all(13),
                child: Image.asset(
                  suffixImg ?? AppImages.icLogo,
                  width: 20,
                  height: 20,
                  fit: BoxFit.fill,
                ),
              )
            : null,
        prefixIcon: prefixImg != null
            ? Padding(
                padding: paddingImg ?? EdgeInsets.only(left: 25, right: 13, top: 13, bottom: 13),
                child: Image.asset(
                  prefixImg,
                  width: 20,
                  height: 20,
                  fit: BoxFit.fill,
                ),
              )
            : null,
        focusedBorder: _borderTextField,
        enabledBorder: _borderTextField,
        errorBorder: _borderTextField,
        focusedErrorBorder: _borderTextField,
        contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: height ?? 16),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: hintStyle ??
            AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.grey,
              fontSize: 16,
            ),
      ),
    );
  }

  _buildBorderTextField({Color color, double circular}) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(circular ?? 150),
      borderSide: BorderSide(
        color: color,
        width: 0.85,
      ),
    );
  }
}
