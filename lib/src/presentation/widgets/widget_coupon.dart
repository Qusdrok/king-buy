import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetCoupon extends StatelessWidget {
  CouponModel couponModel;

  WidgetCoupon({this.couponModel});

  @override
  Widget build(BuildContext context) {
    if (couponModel == null) {
      couponModel = new CouponModel(
        image_source: "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
        name: "Giảm giá 15% sử dụng đơn hàng",
        description: "Giảm giá 15% sử dụng đơn hàng",
        expires_at: "31/07/2019",
      );
    }

    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routers.detail,
        arguments: couponModel,
      ),
      child: Container(
        width: AppSize.screenWidth,
        height: 90,
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          children: [
            WidgetImageNetwork(
              url: couponModel?.image_source ??
                  "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
              fit: BoxFit.fill,
              width: 110,
              height: 85,
            ),
            const SizedBox(width: 12),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    couponModel?.name ?? "Giảm giá 15% sử dụng đơn hàng",
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 8),
                  Text(
                    "HSD: ${couponModel?.expires_at ?? "31/07/2019"}",
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
