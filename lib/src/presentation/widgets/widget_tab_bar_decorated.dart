import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTabBarDecorated extends StatelessWidget implements PreferredSizeWidget {
  WidgetTabBarDecorated({
    @required this.tabBar,
    this.color,
    this.width,
  });

  final TabBar tabBar;
  final Color color;
  final double width;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Container(
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: color ?? AppColors.blue,
                  width: width ?? 2.0,
                ),
              ),
            ),
          ),
        ),
        tabBar,
      ],
    );
  }
}
