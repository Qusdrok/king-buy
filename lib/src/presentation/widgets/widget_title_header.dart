import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTitleHeader extends StatelessWidget {
  final String title;
  final Alignment align;
  final TextStyle style;
  final Function onTap;
  final EdgeInsets padding;

  WidgetTitleHeader({
    this.title,
    this.style,
    this.align,
    this.onTap,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Container(
        padding: padding ?? EdgeInsets.only(left: 15),
        alignment: align ?? Alignment.centerLeft,
        child: Text(
          title,
          style: style ?? AppStyles.DEFAULT_LARGE_BOLD,
        ),
      ),
    );
  }
}
