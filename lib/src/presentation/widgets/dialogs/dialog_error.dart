import 'package:flutter/material.dart';

import '../../../src.dart';

class DialogError extends StatelessWidget {
  final String keyTitle;
  final String keyAction;
  final bool trans;
  final Function action;

  const DialogError({this.keyTitle, this.trans = true, this.action, this.keyAction});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Dialog(
        elevation: 8,
        shape: RoundedRectangleBorder(
          side: BorderSide.none,
          borderRadius: BorderRadius.circular(16),
        ),
        insetPadding: EdgeInsets.only(left: 26, right: 26),
        backgroundColor: HexColor.fromHex("#fdfefe"),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                trans ? AppLocalizations.of(context).translate(keyTitle) : keyTitle,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  fontSize: 16,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            const SizedBox(height: 18),
            InkWell(
              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16)),
              onTap: action ?? () => Navigator.pop(context, true),
              child: Row(
                children: [
                  const SizedBox(width: 0.75),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.primaryDark,
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(16),
                          bottomLeft: Radius.circular(15),
                        ),
                      ),
                      height: 45,
                      child: Center(
                        child: Text(
                          "OK",
                          style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 0.75),
                ],
              ),
            ),
            const SizedBox(height: 0.75),
          ],
        ),
      ),
    );
  }
}
