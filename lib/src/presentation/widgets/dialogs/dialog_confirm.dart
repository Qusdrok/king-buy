import 'package:flutter/material.dart';

import '../../../src.dart';

class DialogConfirm extends StatelessWidget {
  final String keyTitle;
  final String content;
  final bool transTitle;
  final bool transContent;
  final Function actionCancel;
  final Function actionConfirm;

  const DialogConfirm({
    this.keyTitle,
    this.transTitle = true,
    @required this.content,
    this.transContent = false,
    this.actionCancel,
    this.actionConfirm,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 8,
      shape: RoundedRectangleBorder(
        side: BorderSide.none,
        borderRadius: BorderRadius.circular(16),
      ),
      insetPadding: EdgeInsets.only(left: 26, right: 26),
      backgroundColor: HexColor.fromHex("#fdfefe"),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 16),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Text(
              transContent ? AppLocalizations.of(context).translate(content) : content,
              style: AppStyles.DEFAULT_MEDIUM,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 18),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(width: 1),
              Expanded(
                child: Column(
                  children: [
                    InkWell(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(16),
                      ),
                      onTap: () => Navigator.pop(context, false),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.primaryDark,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(16),
                          ),
                        ),
                        height: 45,
                        child: Center(
                          child: Text(
                            "Huỷ bỏ",
                            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 1),
                  ],
                ),
              ),
              const SizedBox(width: 1),
              Expanded(
                child: Column(
                  children: [
                    InkWell(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16)),
                      onTap: actionConfirm ?? () => Navigator.pop(context, true),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.primaryDark,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(16),
                          ),
                        ),
                        height: 45,
                        child: Center(
                          child: Text(
                            "OK",
                            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 1),
                  ],
                ),
              ),
              const SizedBox(width: 1),
            ],
          )
        ],
      ),
    );
  }
}
