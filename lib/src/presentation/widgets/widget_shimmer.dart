import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../src.dart';

class WidgetShimmer extends StatelessWidget {
  final Widget child;
  final MaterialColor color;

  const WidgetShimmer({this.child, this.color});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      child: child,
      baseColor: color ?? Colors.grey.withOpacity(0.25),
      highlightColor: color ?? Colors.white30,
      enabled: true,
    );
  }
}
