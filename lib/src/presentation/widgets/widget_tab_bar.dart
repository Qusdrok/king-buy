import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTabBar extends StatefulWidget {
  final List<String> tabs;
  final Function(int) onTap;
  final EdgeInsets headerPadding;

  WidgetTabBar({
    @required this.tabs,
    this.onTap,
    this.headerPadding,
  });

  @override
  _WidgetTabBarState createState() => _WidgetTabBarState();
}

class _WidgetTabBarState extends State<WidgetTabBar> with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<Widget> _tabs = [];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: widget.tabs.length, vsync: this);
    for (int i = 0; i < widget.tabs.length; i++) {
      _tabs.add(
        _buildTab(
          title: widget.tabs[i],
          index: i,
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      controller: _tabController,
      labelStyle: AppStyles.DEFAULT_MEDIUM,
      isScrollable: false,
      physics: NeverScrollableScrollPhysics(),
      onTap: widget.onTap ?? (index) {},
      labelPadding: widget.headerPadding ?? EdgeInsets.only(top: 15),
      indicator: UnderlineTabIndicator(
        borderSide: BorderSide(
          color: AppColors.red,
          width: 2.5,
        ),
      ),
      tabs: _tabs,
    );
  }

  Widget _buildTab({String title, int index}) {
    return Container(
      width: AppSize.screenWidth / widget.tabs.length - 40,
      padding: EdgeInsets.only(bottom: 10),
      child: Text(
        title,
        style: AppStyles.DEFAULT_MEDIUM_BOLD,
        textAlign: TextAlign.center,
      ),
    );
  }
}
