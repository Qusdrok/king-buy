import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetActionHeader extends StatelessWidget {
  final Function onTap;
  final String url;
  final double height;
  final double width;
  final int amount;

  WidgetActionHeader({
    this.onTap,
    @required this.url,
    this.height,
    this.width,
    this.amount,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap ?? () {},
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Image.asset(
            url,
            width: width ?? 26,
            height: height ?? 28,
            fit: BoxFit.fill,
          ),
          if ((amount ?? 0) > 0)
            Positioned(
              right: -5,
              top: -2,
              child: Container(
                width: 15,
                height: 15,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.85,
                    color: Colors.white,
                  ),
                  shape: BoxShape.circle,
                  color: AppColors.red,
                ),
                alignment: Alignment.center,
                child: Text(
                  amount.toString(),
                  style: AppStyles.DEFAULT_SMALL.copyWith(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
