import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetTextMoney extends StatelessWidget {
  final int money;
  final TextStyle textStyle;
  final MainAxisAlignment align;
  final bool isNoExpanded;

  WidgetTextMoney({
    this.money,
    this.textStyle,
    this.align,
    this.isNoExpanded = false,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: align ?? MainAxisAlignment.end,
      children: [
        isNoExpanded
            ? Text(
                AppUtils.convertNumber(money),
                style: textStyle,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            : Flexible(
                child: Text(
                  AppUtils.convertNumber(money),
                  style: textStyle,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
        Transform.translate(
          offset: const Offset(2, -4),
          child: Text(
            'đ',
            textScaleFactor: 0.7,
            style: textStyle,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
