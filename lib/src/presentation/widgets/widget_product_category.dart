import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/presentation/widgets/widget_product_path.dart';

import '../../src.dart';

class WidgetProductCategory extends StatefulWidget {
  final CategoryModel categoryModel;
  final int id;
  final double width;
  final Axis axis;
  final Widget loadMore;
  final EdgeInsets padding;
  final bool showTitle;
  final bool showPath;
  final bool showMore;
  final Function(int childId) onTapTab;

  WidgetProductCategory({
    @required this.categoryModel,
    this.id,
    this.padding,
    this.width,
    this.showTitle = true,
    this.showPath = false,
    this.showMore = true,
    this.onTapTab,
    this.axis,
    this.loadMore,
  });

  @override
  _WidgetProductCategoryState createState() => _WidgetProductCategoryState();
}

class _WidgetProductCategoryState extends State<WidgetProductCategory> {
  List<CategoryModel> _tabs = [CategoryModel(name: "Tất cả", id: 0)];
  int _tabSelected = 0;

  @override
  void initState() {
    super.initState();
    if (widget.categoryModel != null) _tabs.addAll(widget.categoryModel.children);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.showTitle) WidgetTitleHeader(title: widget.categoryModel?.name ?? "Ghế massage"),
        if (widget.showPath) WidgetProductPath(model: widget.categoryModel),
        const SizedBox(height: 15),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: WidgetImageNetwork(
            url: widget.categoryModel != null
                ? AppUtils.convertUrl(widget.categoryModel.image_source)
                : "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
            fit: BoxFit.fill,
            width: AppSize.screenWidth,
            height: AppSize.screenWidth / 2,
          ),
        ),
        const SizedBox(height: 15),
        _buildTabs(_tabs),
        const SizedBox(height: 15),
        WidgetProductItems(
          api: AppEndpoint.GET_PRODUCT_BY_CATEGORY,
          productId: widget.id,
          title: "products",
          padding: widget.padding,
          width: widget.width,
          axis: widget.axis,
          loadMore: widget.loadMore,
        ),
        if (widget.showMore)
          WidgetTitleHeader(
            onTap: () => Navigator.pushNamed(
              context,
              Routers.category_product,
              arguments: widget.categoryModel,
            ),
            title: "Xem thêm",
            align: Alignment.center,
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
              color: AppColors.blue,
              decoration: TextDecoration.underline,
            ),
          ),
        const SizedBox(height: 15),
      ],
    );
  }

  Widget _buildTabs(List<CategoryModel> tabs) {
    return Container(
      width: AppSize.screenWidth,
      child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        physics: BouncingScrollPhysics(),
        child: Wrap(
          spacing: 10,
          children: List.generate(
            tabs.length,
            (index) => _buildTabTile(tabs, index),
          ),
        ),
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  Widget _buildTabTile(List<CategoryModel> tabs, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _tabSelected = index;
        });
      },
      child: PhysicalModel(
        borderRadius: BorderRadius.circular(8.0),
        color: AppColors.grey.withOpacity(0.5),
        elevation: 1,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(
              color: _tabSelected == index ? Colors.transparent : AppColors.black,
              width: 0.1,
            ),
            color: _tabSelected == index ? AppColors.blue : Colors.white,
          ),
          child: Text(
            tabs[index].name,
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: _tabSelected == index ? AppColors.white : AppColors.black,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    );
  }
}
