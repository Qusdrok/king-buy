import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetShopAddress extends StatelessWidget {
  final ShopModel shopModel;
  final bool isHidePhone;

  WidgetShopAddress({this.shopModel, this.isHidePhone = false});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Image.asset(
          AppImages.icPin,
          width: 15,
          height: 20,
          fit: BoxFit.fill,
        ),
        const SizedBox(width: 5),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                shopModel.address,
                style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                  color: AppColors.red,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 5),
              if (!isHidePhone)
                Row(
                  children: [
                    Image.asset(
                      AppImages.icPhone,
                      width: 12,
                      height: 12,
                      fit: BoxFit.fill,
                    ),
                    const SizedBox(width: 5),
                    Text(
                      shopModel.hot_line,
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: AppColors.blue,
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ],
    );
  }
}
