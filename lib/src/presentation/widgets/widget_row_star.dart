import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetRowStar extends StatefulWidget {
  final double star;

  WidgetRowStar({@required this.star});

  @override
  _WidgetRowStarState createState() => _WidgetRowStarState();
}

class _WidgetRowStarState extends State<WidgetRowStar> {
  int _star;

  @override
  void initState() {
    super.initState();
    _star = widget.star.toInt();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Wrap(
          spacing: 3,
          children: List.generate(
            _star == 0 ? 5 : _star,
            (index) => Icon(
              Icons.star,
              size: 18,
              color: widget.star == 0 ? Colors.grey[300] : AppColors.orangeLight,
            ),
          ),
        ),
        const SizedBox(width: 3),
        if ((widget.star - _star) / 0.5 == 1)
          WidgetFilledHalf(
            icon: Icons.star,
            size: 18,
            color: AppColors.orangeLight,
          ),
        const SizedBox(width: 3),
        if (5 - _star > 0 && widget.star > 0)
          Wrap(
            spacing: 3,
            children: List.generate(
              5 - _star - 1,
              (index) => Icon(
                Icons.star,
                size: 18,
                color: Colors.grey[300],
              ),
            ),
          )
      ],
    );
  }
}
