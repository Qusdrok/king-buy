import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class WidgetCarousel extends StatefulWidget {
  final List<String> images;
  final double widthImage;
  final double heightImage;
  final BoxFit fitImage;
  final double bottomPaddingDot;
  final double circular;
  final bool isOnline;
  final Function(int index) onPageChanged;

  WidgetCarousel({
    this.images,
    this.isOnline = false,
    this.widthImage,
    this.heightImage,
    this.circular = 5,
    this.fitImage,
    this.bottomPaddingDot,
    this.onPageChanged,
  });

  @override
  _WidgetCarouselState createState() => _WidgetCarouselState();
}

class _WidgetCarouselState extends State<WidgetCarousel> {
  int _tabSelected = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        CarouselSlider(
          items: List.generate(
            widget.images.length,
            (index) => widget.isOnline
                ? WidgetImageNetwork(
                    borderRadius: BorderRadius.all(Radius.circular(widget.circular)),
                    url: AppUtils.convertUrl(widget.images[index]),
                    fit: BoxFit.fill,
                    width: AppSize.screenWidth,
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(widget.circular)),
                    child: Image.asset(
                      widget.images[index],
                      width: widget.widthImage ?? AppSize.screenWidth,
                      fit: widget.fitImage ?? BoxFit.fill,
                    ),
                  ),
          ),
          options: CarouselOptions(
            height: widget.heightImage ?? 300,
            viewportFraction: 1,
            enableInfiniteScroll: true,
            initialPage: 1,
            aspectRatio: 2,
            autoPlay: false,
            enlargeCenterPage: false,
            onPageChanged: (index, _) {
              setState(() {
                _tabSelected = index;
                if (widget.onPageChanged != null) widget.onPageChanged(index);
              });
            },
          ),
        ),
        Positioned(
          bottom: widget.bottomPaddingDot ?? 52,
          right: 5,
          child: Row(
            children: List.generate(
              widget.images.length,
              (index) => _buildDot(index),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDot(int index) {
    bool isChange = _tabSelected == index;
    return Container(
      width: 12,
      height: 12,
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 2),
      decoration: BoxDecoration(
        border: Border.all(
          width: 0.85,
          color: Colors.white,
        ),
        borderRadius: BorderRadius.all(Radius.circular(15)),
        color: isChange ? AppColors.white : Colors.transparent,
      ),
    );
  }
}
