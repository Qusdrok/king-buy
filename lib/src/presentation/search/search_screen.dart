import 'package:flutter/material.dart';

import '../../src.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  ScrollController _scrollController = ScrollController();
  SearchViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.maxScrollExtent == _scrollController.offset) {}
    });
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<SearchViewModel>(
      viewModel: SearchViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                _buildAppBar(10),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildAppBar(double height) {
    return Container(
      width: AppSize.screenWidth,
      height: AppValues.HEIGHT_APP_BAR + (height ?? 0),
      color: AppColors.blue,
      padding: const EdgeInsets.only(left: 20, right: 10, bottom: 5, top: 5),
      child: Row(
        children: [
          Expanded(
            child: WidgetTextField(
              controller: _viewModel.searchController,
              height: 0,
              onSubmitted: _viewModel.controlSearching,
              colorBorder: Colors.transparent,
              circular: 10,
              hintText: "Tìm kiếm ...",
              prefixImg: AppImages.icSearch,
            ),
          ),
          const SizedBox(width: 10),
          _buildCancel(),
        ],
      ),
    );
  }

  Widget _buildCancel() {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Text(
        "Huỷ",
        style: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.white,
        ),
      ),
    );
  }

  Widget _buildBody() {
    return StreamBuilder(
      stream: AppShared.watchHistorySearch(),
      builder: (context, snapshot) {
        bool enabled = !snapshot.hasData;
        List<String> data = snapshot.data;

        if (!enabled) _viewModel.historySearch = data;
        if (enabled) {
          return WidgetShimmer(
            child: ListView.separated(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 20),
              itemBuilder: (context, index) => Padding(
                padding: EdgeInsets.only(left: 50),
                child: Text(
                  "Lịch sử",
                  style: AppStyles.DEFAULT_MEDIUM,
                ),
              ),
              separatorBuilder: (context, index) => Container(height: 15),
              itemCount: 5,
            ),
          );
        } else {
          return data.length < 1
              ? Container()
              : Container(
                  width: AppSize.screenWidth,
                  height: 50.0 * data.length + 70,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView.separated(
                          physics: BouncingScrollPhysics(),
                          shrinkWrap: true,
                          padding: EdgeInsets.only(top: 20),
                          itemBuilder: (context, index) => GestureDetector(
                            onTap: () => Navigator.pushNamed(
                              context,
                              Routers.category_product,
                              arguments: data[index],
                            ),
                            child: Padding(
                              padding: EdgeInsets.only(left: 50),
                              child: Text(
                                data[index],
                                style: AppStyles.DEFAULT_MEDIUM,
                              ),
                            ),
                          ),
                          separatorBuilder: (context, index) => Divider(
                            color: AppColors.grey,
                            thickness: 0.75,
                            indent: 50,
                            endIndent: 50,
                          ),
                          itemCount: data.length,
                        ),
                      ),
                      const SizedBox(height: 30),
                      WidgetTitleHeader(
                        onTap: () => _viewModel.deleteHistorySearch(),
                        title: "Xoá lịch sử tìm kiếm",
                        align: Alignment.center,
                        style: AppStyles.DEFAULT_MEDIUM,
                      ),
                      const SizedBox(height: 30),
                    ],
                  ),
                );
        }
      },
    );
  }
}
