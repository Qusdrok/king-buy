import 'package:flutter/material.dart';

import '../../src.dart';

class SearchViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  List<String> historySearch = [];

  init() {}

  deleteHistorySearch() async => await AppShared.setHistorySearch(null);

  controlSearching(String keyword) async {
    historySearch.add(keyword);
    await AppShared.setHistorySearch(historySearch);
    Navigator.pushReplacementNamed(context, Routers.category_product, arguments: keyword);
  }
}
