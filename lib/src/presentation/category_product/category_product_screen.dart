import 'package:flutter/material.dart';

import '../../src.dart';

class CategoryProductScreen extends StatefulWidget {
  final dynamic model;

  CategoryProductScreen({@required this.model});

  @override
  _CategoryProductScreenState createState() => _CategoryProductScreenState();
}

class _CategoryProductScreenState extends State<CategoryProductScreen> {
  CategoryProductViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CategoryProductViewModel>(
      viewModel: CategoryProductViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          key: _viewModel.scaffoldKey,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  height: 10,
                  title: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: WidgetTextField(
                      onTap: () => Navigator.pushReplacementNamed(context, Routers.search),
                      controller: _viewModel.searchController,
                      height: 0,
                      colorBorder: Colors.transparent,
                      circular: 10,
                      hintText: widget.model is CategoryModel ? "Tìm kiếm ..." : widget.model,
                      prefixImg: AppImages.icSearch,
                    ),
                  ),
                  isSpace: true,
                  actions: [
                    const SizedBox(width: 10),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          WidgetActionHeader(
                            onTap: () => _viewModel.openEndDrawer(),
                            url: AppImages.icFilterWhite,
                            height: 25,
                          ),
                          Text(
                            "Lọc",
                            style: AppStyles.DEFAULT_MEDIUM.copyWith(
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    const SizedBox(width: 15),
                  ],
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
          endDrawer: _buildEndDrawer(),
          endDrawerEnableOpenDragGesture: false,
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: widget.model is CategoryModel
          ? Column(
              children: [
                WidgetProductCategory(
                  categoryModel: widget.model,
                  showPath: true,
                  showTitle: false,
                  showMore: false,
                  axis: Axis.vertical,
                  width: AppSize.screenWidth / 2 - 15,
                  loadMore: _buildMoreProduct(),
                  id: widget.model.id,
                ),
                _buildDescription(),
              ],
            )
          : WidgetProductItems(
              api: AppEndpoint.GET_SEARCH_PRODUCT,
              width: AppSize.screenWidth / 2 - 15,
              padding: EdgeInsets.symmetric(vertical: 10),
              axis: Axis.vertical,
              searchWord: widget.model,
              loadMore: _buildMoreProduct(),
            ),
    );
  }

  Widget _buildDescription() {
    return Column(
      children: [
        WidgetTitleHeader(title: widget.model.name),
        Padding(
          padding: const EdgeInsets.only(left: 13, right: 10, bottom: 10, top: 5),
          child: Text(
            "Xin chào bạn ! Với cam kết của KingBuy tất cả các sản phẩm được giao hàng, lắp đặt, trải nghiệm miễn phí.\n"
            "Hỗ trợ giao hàng, lắp đặt miễn phí tại nhà trên toàn quốc. Trải nghiệm miễn phí tại showroom trên toàn quốc ạ.",
            style: AppStyles.DEFAULT_MEDIUM,
          ),
        ),
      ],
    );
  }

  Widget _buildEndDrawer({bool isColumn = false}) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: AppValues.HEIGHT_APP_BAR + 10,
              color: AppColors.blue,
              padding: EdgeInsets.only(left: 15),
              child: Row(
                children: [
                  WidgetActionHeader(
                    url: AppImages.icFilterWhite,
                    height: 25,
                  ),
                  const SizedBox(width: 15),
                  Text(
                    "BỘ LỌC TÌM KIẾM",
                    style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            if (!isColumn) _buildColumnEndDrawer() else _buildTabEndDrawer(),
            _buildButtonEndDrawer(),
          ],
        ),
      ),
    );
  }

  Widget _buildTabEndDrawer() {
    return Column(
      children: [
        WidgetTabBar(
          tabs: [
            "Danh mục",
            "Thương hiệu",
            "Giá",
          ],
        ),
        Container(
          height: AppSize.screenHeight - 185,
          child: Text("abc"),
        ),
      ],
    );
  }

  Widget _buildColumnEndDrawer() {
    return Column(
      children: [
        const SizedBox(height: 10),
        WidgetTitleHeader(title: "Danh mục"),
        const SizedBox(height: 20),
        _buildPromotions(),
        Divider(color: AppColors.grey),
        const SizedBox(height: 10),
        WidgetTitleHeader(title: "Thương hiệu"),
        const SizedBox(height: 20),
        Divider(color: AppColors.grey),
        const SizedBox(height: 10),
        WidgetTitleHeader(title: "Giá"),
        const SizedBox(height: 20),
        _buildPrices(),
      ],
    );
  }

  Widget _buildMoreProduct() {
    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: Container(
        width: 110,
        height: 40,
        decoration: BoxDecoration(
          color: AppColors.blue,
          borderRadius: BorderRadius.all(Radius.circular(40)),
        ),
        alignment: Alignment.center,
        child: Text(
          "Xem thêm",
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildPromotions() {
    return Column(
      children: [
        Wrap(
          spacing: 20,
          runSpacing: 10,
          alignment: WrapAlignment.center,
          children: List.generate(
            DataFilter.categories.length,
            (index) => index >= 4
                ? Container()
                : GestureDetector(
                    onTap: () => _viewModel.setSelected(SelectedType.Category, index),
                    child: _buildButton(
                      title: DataFilter.categories[index].name,
                      isSelected: _viewModel.categorySelected == index,
                    ),
                  ),
          ),
        ),
        _buildMore(),
        const SizedBox(height: 15),
      ],
    );
  }

  Widget _buildPrices() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Wrap(
            spacing: 20,
            runSpacing: 10,
            children: List.generate(
              DataFilter.prices.length - 6,
              (index) => GestureDetector(
                onTap: () => _viewModel.setSelected(SelectedType.Price, index),
                child: _buildButton(
                  title: "${DataFilter.prices[index].from} - ${DataFilter.prices[index].to} triệu",
                  isSelected: _viewModel.priceSelected == index,
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Text(
            "Hoặc nhập giá ở ô dưới",
            style: AppStyles.DEFAULT_MEDIUM,
          ),
        ),
        const SizedBox(height: 5),
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(
                child: PhysicalModel(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: AppColors.grey,
                  elevation: 5,
                  child: WidgetTextField(
                    controller: _viewModel.fromController,
                    circular: 5,
                    height: 0,
                    hintText: "Từ 0đ",
                  ),
                ),
              ),
              const SizedBox(width: 20),
              Expanded(
                child: PhysicalModel(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: AppColors.grey,
                  elevation: 5,
                  child: WidgetTextField(
                    controller: _viewModel.toController,
                    circular: 5,
                    height: 0,
                    hintText: "Đến 0đ",
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 20),
      ],
    );
  }

  Widget _buildButtonEndDrawer() {
    return Container(
      height: 60,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: AppColors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
          ),
        ],
        color: Colors.white,
      ),
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          _buildEndButton(
            onTap: () => _viewModel.reset ? _viewModel.resetSelected() : {},
            text: "Thiết lập lại",
            colorButton: _viewModel.reset ? AppColors.red : AppColors.grey,
          ),
          const SizedBox(width: 10),
          _buildEndButton(text: "Áp dụng"),
        ],
      ),
    );
  }

  Widget _buildEndButton({Function onTap, Color colorButton, String text}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 100,
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(35)),
          color: colorButton ?? AppColors.red,
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildButton({String title, bool isSelected = false}) {
    return Container(
      width: 120,
      height: 50,
      child: Stack(
        children: [
          Image.asset(
            isSelected ? AppImages.imgBgDrawerItemSelected : AppImages.imgBgDrawerItem,
            width: 120,
            height: 50,
            fit: BoxFit.fill,
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Text(
                title ?? "Thể loại",
                style: AppStyles.DEFAULT_MEDIUM,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMore() {
    return Container(
      width: 120,
      height: 40,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20)),
        border: Border.all(
          color: AppColors.blue,
          width: 0.5,
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Xem thêm",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.blue,
              decoration: TextDecoration.underline,
            ),
          ),
          const SizedBox(width: 3),
          Transform.rotate(
            angle: 90 * AppValues.PI / 180,
            child: Image.asset(
              AppImages.icChevron,
              width: 8,
              height: 12,
              fit: BoxFit.fill,
              color: AppColors.blue,
            ),
          ),
        ],
      ),
    );
  }
}
