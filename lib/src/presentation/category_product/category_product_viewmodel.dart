import 'package:flutter/material.dart';

import '../../src.dart';

enum SelectedType { Category, Price, Brand }

class CategoryProductViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  TextEditingController fromController = TextEditingController();
  TextEditingController toController = TextEditingController();
  TextEditingController Controller = TextEditingController();
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  bool reset = false;
  int categorySelected = -1;
  int priceSelected = -1;

  init() {}

  setSelected(SelectedType selectedType, int index) {
    switch (selectedType) {
      case SelectedType.Category:
        if (categorySelected == index) {
          categorySelected = -1;
        } else {
          reset = true;
          categorySelected = index;
        }
        break;
      case SelectedType.Price:
        if (priceSelected == index) {
          priceSelected = -1;
        } else {
          priceSelected = index;
          reset = true;
        }
        break;
      case SelectedType.Brand:
        break;
    }

    if (categorySelected == -1 && priceSelected == -1 && fromController.text.isEmpty && toController.text.isEmpty)
      reset = false;
    notifyListeners();
  }

  resetSelected() {
    reset = false;

    categorySelected = -1;
    priceSelected = -1;

    fromController.clear();
    toController.clear();

    notifyListeners();
  }

  openEndDrawer() => scaffoldKey.currentState.openEndDrawer();
}
