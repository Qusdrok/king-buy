import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class EnterOTPViewModel extends BaseViewModel {
  TextEditingController pinController = TextEditingController();
  String verificationID;
  String account;
  bool isEmail;

  EnterOTPViewModel({this.account});

  init() {
    isEmail = AppValid.isLoginWithEmailOrPhoneNumber(account) == AccountType.Email;
    sendOTP();
  }

  Future<void> sendOTP({bool isAgain = false}) async {
    if (isEmail) {
      AppFirebase.firebaseAuth.currentUser.sendEmailVerification();
    } else {
      verifyPhoneNumber();
    }

    if (isAgain) await handleConfirm(msg: "Đã gửi lại mã otp");
  }

  Future<void> signIn() async {
    try {
      setLoading(true);
      if (isEmail) {
        await AppFirebase.firebaseAuth.currentUser.reload();
        if (AppFirebase.firebaseAuth.currentUser.emailVerified) {
          setLoading(false);
          await handleConfirm(msg: "Đăng ký thành công");
          Navigator.pushReplacementNamed(context, Routers.navigation);
        }
      } else {
        await AppFirebase.firebaseAuth
            .signInWithCredential(
          PhoneAuthProvider.credential(
            verificationId: this.verificationID,
            smsCode: pinController.text,
          ),
        )
            .then((value) async {
          if (value.user != null) {
            setLoading(false);
            await handleConfirm(msg: "Đăng ký thành công");
            Navigator.pushReplacementNamed(context, Routers.navigation);
          } else {
            setLoading(false);
          }
        });
      }
    } catch (e) {
      pinController.clear();
      unFocus();
      setLoading(false);
      await handleConfirm(msg: "Mã xác minh bạn nhập vào không đúng");
    }
  }

  void verifyPhoneNumber() async {
    await AppFirebase.firebaseAuth.verifyPhoneNumber(
      phoneNumber: AppUtils.convertPhoneNumber(account, isHide: false),
      verificationCompleted: (credential) => print("Đăng ký thành công"),
      timeout: const Duration(seconds: 60),
      verificationFailed: (e) async {
        setLoading(false);
        await handleConfirm(msg: "Xác minh thất bại");
      },
      codeSent: (verificationID, resendToken) async {
        this.verificationID = verificationID;
      },
      codeAutoRetrievalTimeout: (verificationID) async {
        setLoading(false);
        await handleConfirm(msg: "Code đã hết hạn sử dụng");
      },
    );
  }
}
