import 'package:flutter/material.dart';
import 'package:pinput/pin_put/pin_put.dart';

import '../../src.dart';

class EnterOTPScreen extends StatefulWidget {
  final String account;

  EnterOTPScreen({this.account});

  @override
  _EnterOTPScreenState createState() => _EnterOTPScreenState();
}

class _EnterOTPScreenState extends State<EnterOTPScreen> {
  EnterOTPViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<EnterOTPViewModel>(
      viewModel: EnterOTPViewModel(account: widget.account),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Nhập mã xác nhận",
                  alignTitle: Alignment.centerLeft,
                  trans: false,
                ),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: AppSize.screenWidth,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              color: Colors.transparent,
              child: _buildEnterOTP(loading),
            );
          },
        ),
      ),
    );
  }

  Widget _buildEnterOTP(bool loading) {
    return Column(
      children: [
        SizedBox(height: 25),
        Text(
          "Nhập mã được gửi tới",
          style: AppStyles.DEFAULT_LARGE.copyWith(
            color: AppColors.red,
            fontSize: 24,
          ),
        ),
        Text(
          widget.account.contains(".com")
              ? AppUtils.convertGmail(widget.account)
              : AppUtils.convertPhoneNumber(widget.account),
          style: AppStyles.DEFAULT_MEDIUM.copyWith(
            color: AppColors.grey,
            fontSize: 25,
          ),
        ),
        const SizedBox(height: 15),
        _buildPin(),
        const SizedBox(height: 15),
        GestureDetector(
          onTap: () => _viewModel.signIn(),
          child: Container(
            width: AppSize.screenWidth,
            height: 50,
            color: AppColors.grey.withOpacity(0.25),
            alignment: Alignment.center,
            child: Text(
              "Tiếp tục",
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.red,
              ),
            ),
          ),
        ),
        const SizedBox(height: 15),
        InkWell(
          onTap: () => _viewModel.sendOTP(isAgain: true),
          child: Text(
            "Tôi không nhận được mã",
            style: AppStyles.DEFAULT_LARGE.copyWith(
              color: AppColors.grey.withOpacity(0.25),
              fontSize: 24,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPin() {
    final BoxDecoration pinPutDecoration = BoxDecoration(color: AppColors.grey.withOpacity(0.25));
    return PinPut(
      fieldsCount: 6,
      eachFieldHeight: 40,
      eachFieldConstraints: BoxConstraints(minHeight: 45, minWidth: 50),
      withCursor: true,
      controller: _viewModel.pinController,
      onSubmit: (pin) => _viewModel.signIn(),
      submittedFieldDecoration: pinPutDecoration,
      selectedFieldDecoration: pinPutDecoration,
      followingFieldDecoration: pinPutDecoration,
    );
  }
}
