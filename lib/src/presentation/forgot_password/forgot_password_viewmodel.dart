import 'package:flutter/cupertino.dart';

import '../../src.dart';

class ForgotPasswordViewModel extends BaseViewModel {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  GlobalKey<FormState> formForgotPassword = GlobalKey<FormState>();

  bool isFillPassword = false;

  init() {}

  switchScreen() {
    isFillPassword = !isFillPassword;
    notifyListeners();
  }

  @override
  void dispose() async {
    usernameController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
    super.dispose();
  }
}
