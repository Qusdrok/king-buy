import 'package:flutter/material.dart';

import '../../src.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  ForgotPasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<ForgotPasswordViewModel>(
      viewModel: ForgotPasswordViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "${!_viewModel.isFillPassword ? "Lấy" : "Tạo"} lại mật khẩu",
                  alignTitle: Alignment.centerLeft,
                  trans: false,
                  actionBack: () => !_viewModel.isFillPassword ? Navigator.pop(context) : _viewModel.switchScreen(),
                ),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) => RotationTransition(turns: animation, child: child),
      layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
      switchInCurve: Curves.easeInBack,
      switchOutCurve: Curves.easeInBack.flipped,
      duration: Duration(seconds: 1),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return Container(
              width: AppSize.screenWidth,
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: !_viewModel.isFillPassword ? _buildFillAccount() : _buildFillPassword(loading),
            );
          },
        ),
      ),
    );
  }

  Widget _buildFillAccount() {
    return Column(
      children: [
        SizedBox(height: AppSize.screenWidth / 3),
        Text(
          "NHẬP THÔNG TIN TÀI KHOẢN",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        const SizedBox(height: 30),
        Form(
          key: _viewModel.formForgotPassword,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              WidgetTextField(
                controller: _viewModel.usernameController,
                hintText: "Họ và tên",
                prefixImg: AppImages.icProfile2,
              ),
              const SizedBox(height: 15),
              WidgetButtonGradientAnim(
                width: AppSize.screenWidth / 2 - 30,
                title: "Tiếp theo",
                trans: false,
                action: () => _viewModel.switchScreen(),
                colorStart: AppColors.red,
                colorEnd: AppColors.red,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildFillPassword(bool loading) {
    return Column(
      children: [
        SizedBox(height: AppSize.screenWidth / 3),
        Text(
          "MẬT KHẨU MỚI",
          style: AppStyles.DEFAULT_LARGE_BOLD,
        ),
        const SizedBox(height: 30),
        Form(
          key: _viewModel.formForgotPassword,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              WidgetTextField(
                controller: _viewModel.passwordController,
                hintText: "Mật khẩu",
                prefixImg: AppImages.icLock2,
              ),
              const SizedBox(height: 10),
              WidgetTextField(
                controller: _viewModel.confirmPasswordController,
                hintText: "Nhập lại mật khẩu",
                prefixImg: AppImages.icLock2,
              ),
              const SizedBox(height: 15),
              WidgetButtonGradientAnim(
                width: AppSize.screenWidth / 2 - 30,
                title: "Cập nhật",
                trans: false,
                action: () {},
                colorStart: AppColors.red,
                colorEnd: AppColors.red,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
