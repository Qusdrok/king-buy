import 'package:flutter/material.dart';

import '../../src.dart';

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  NavigationViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NavigationViewModel>(
      viewModel: NavigationViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return WidgetFadeTransitions(
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                Expanded(child: _buildPages()),
                _buildBottomNavBar(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildButton({int index, String imgUrl, String title}) {
    return Expanded(
      child: InkWell(
        onTap: () => _viewModel.switchPage(index),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              imgUrl,
              width: 28,
              height: 28,
              color: _viewModel.currentPage == index ? AppColors.red : AppColors.grey,
            ),
            const SizedBox(height: 10),
            Text(
              title,
              style: AppStyles.DEFAULT_SMALL.copyWith(
                color: _viewModel.currentPage == index ? AppColors.red : AppColors.grey,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomNavBar() {
    return Container(
      width: AppSize.screenWidth,
      padding: EdgeInsets.only(top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: AppColors.grey,
            blurRadius: 1.2,
          ),
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        overflow: Overflow.visible,
        children: [
          Row(
            children: [
              _buildButton(
                index: 0,
                imgUrl: AppImages.icHomeGrey,
                title: "Trang chủ",
              ),
              _buildButton(
                index: 1,
                imgUrl: AppImages.icMenuOff,
                title: "Danh mục",
              ),
              Expanded(child: Container()),
              _buildButton(
                index: 2,
                imgUrl: AppImages.icAlarmOff,
                title: "Thông báo",
              ),
              _buildButton(
                index: 3,
                imgUrl: AppImages.icHomeGrey,
                title: "Tài khoản",
              ),
            ],
          ),
          Positioned(
            top: -15,
            child: GestureDetector(
              onTap: () => Navigator.pushNamed(context, Routers.card_member),
              child: CircleAvatar(
                radius: 30,
                backgroundColor: Colors.transparent,
                child: Image.asset(
                  AppImages.icRadioGroupSelected,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPages() {
    switch (_viewModel.currentPage) {
      case 0:
        return HomeScreen(
          onTapAccount: () => _viewModel.switchPage(3),
          onTapCategory: () => _viewModel.switchPage(1),
        );
      case 1:
        return CategoryScreen();
      case 2:
        return NotificationScreen();
      case 3:
        return AccountScreen();
      default:
        return SizedBox();
    }
  }
}
