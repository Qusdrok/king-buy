import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_app_king_buy/src/presentation/presentation.dart';
import 'package:flutter_app_king_buy/src/presentation/widgets/widget_action_header.dart';

import '../../src.dart';

class HomeScreen extends StatefulWidget {
  final Function onTapAccount;
  final Function onTapCategory;

  HomeScreen({
    @required this.onTapAccount,
    @required this.onTapCategory,
  });

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  HomeViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<HomeViewModel>(
      viewModel: HomeViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Stack(
            children: [
              Column(
                children: [
                  _buildHeader(false),
                  Expanded(child: _buildBody()),
                ],
              ),
              _buildFloatingButton(),
              if (DataFilter.popup != null) _buildPopUp(),
            ],
          ),
        );
      },
    );
  }

  Widget _buildFloatingButton() {
    return Positioned(
      bottom: AppSize.screenWidth / 6,
      right: 0,
      child: IconButton(
        icon: Image.asset(
          AppImages.icCall3,
          fit: BoxFit.fill,
        ),
        iconSize: 75,
        onPressed: () {},
      ),
    );
  }

  Widget _buildHeader(bool isLogin) {
    return isLogin
        ? WidgetAppBar(
            height: 10,
            align: MainAxisAlignment.center,
            leading: WidgetLogo(width: 50, height: 50),
            title: WidgetTextField(
              height: 10,
              controller: _viewModel.searchController,
              hintText: "Tìm kiếm",
              suffixImg: AppImages.icSearch,
            ),
            isSpace: true,
            actions: [
              WidgetActionHeader(url: AppImages.icGps, height: 35),
              const SizedBox(width: 15),
              WidgetActionHeader(url: AppImages.icShoppingCart, amount: 1),
              const SizedBox(width: 10),
            ],
          )
        : WidgetAppBar(
            height: 10,
            align: MainAxisAlignment.center,
            leading: Container(),
            title: StreamBuilder(
              stream: AppShared.watchUser(),
              builder: (context, snapshot) {
                bool enabled = !snapshot.hasData;
                UserModel data = snapshot.data;

                return enabled
                    ? WidgetShimmer(
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: WidgetImageNetwork(
                                url: "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
                                fit: BoxFit.fill,
                                height: 50,
                                width: 50,
                                shape: ImageNetworkShape.circle,
                              ),
                            ),
                            const SizedBox(width: 15),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Text(
                                    "Nguyễn Văn A",
                                    style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                      color: AppColors.white,
                                    ),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                                Expanded(
                                  child: Text(
                                    "0 điểm",
                                    style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                                      color: AppColors.white,
                                    ),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                    : Row(
                        children: [
                          GestureDetector(
                            onTap: widget.onTapAccount ?? () {},
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: WidgetImageNetwork(
                                url: AppUtils.convertUrl(data.avatar_source),
                                fit: BoxFit.fill,
                                height: 50,
                                width: 50,
                                shape: ImageNetworkShape.circle,
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                data.name,
                                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                  color: AppColors.white,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              Text(
                                "${data.reward_points} điểm",
                                style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                                  color: AppColors.white,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ],
                          ),
                        ],
                      );
              },
            ),
            isSpace: true,
            actions: [
              WidgetActionHeader(
                url: AppImages.icSearchWhite,
                height: 30,
                onTap: () => Navigator.pushNamed(context, Routers.search),
              ),
              const SizedBox(width: 20),
              WidgetActionHeader(url: AppImages.icGps, height: 33, width: 25),
              const SizedBox(width: 20),
              WidgetActionHeader(url: AppImages.icGift, height: 30, amount: 2),
              const SizedBox(width: 20),
              WidgetActionHeader(
                url: AppImages.icShoppingCart,
                amount: 1,
                onTap: () => Navigator.pushNamed(context, Routers.shop),
              ),
              const SizedBox(width: 10),
            ],
          );
  }

  Widget _buildBody() {
    return WidgetLoadMore(
      header: Column(
        children: [
          _buildCarousel(),
          const SizedBox(height: 15),
          WidgetTitleHeader(title: "Danh mục hot"),
          const SizedBox(height: 15),
          _buildHotCategories(),
          const SizedBox(height: 20),
          WidgetTitleHeader(title: "Ưu đãi dành cho bạn"),
          const SizedBox(height: 15),
          Container(
            width: AppSize.screenWidth,
            height: 200,
            child: WidgetLoadMore(
              itemBuilder: _buildMyPromotions,
              dataRequester: _viewModel.dataMyPromotion,
              initRequester: _viewModel.initMyPromotion,
              limit: 2,
              spaceBetweenWidget: 15,
              autoLoadMore: true,
              padding: EdgeInsets.symmetric(horizontal: 15),
            ),
          ),
          const SizedBox(height: 20),
          WidgetTitleHeader(title: "Sản phẩm mới"),
          const SizedBox(height: 10),
          WidgetProductItems(
            api: AppEndpoint.GET_ALL_NEW_PRODUCT,
            padding: EdgeInsets.symmetric(horizontal: 10),
          ),
          const SizedBox(height: 10),
          WidgetTitleHeader(title: "Sản phẩm bán chạy"),
          const SizedBox(height: 10),
          WidgetProductItems(
            api: AppEndpoint.GET_SELLING_PRODUCT,
            padding: EdgeInsets.symmetric(horizontal: 10),
          ),
        ],
      ),
      itemBuilder: _buildCategories,
      dataRequester: _viewModel.dataCategories,
      initRequester: _viewModel.initCategories,
      autoLoadMore: true,
      limit: 1,
      isSingleWrap: true,
      axis: Axis.vertical,
    );
  }

  Widget _buildCategories(List data, BuildContext context, int index) {
    return WidgetProductCategory(
      categoryModel: data[index],
      id: data[index]?.id ?? 1,
      padding: EdgeInsets.symmetric(horizontal: 8),
    );
  }

  Widget _buildMyPromotions(List data, BuildContext context, int index) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        Routers.detail,
        arguments: data[index],
      ),
      child: WidgetImageNetwork(
        borderRadius: BorderRadius.all(Radius.circular(15)),
        url: AppUtils.convertUrl(data[index]?.image_source ?? ""),
        width: 280,
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _buildHotCategories() {
    List<CategoryModel> categories = [CategoryModel(image_source: AppImages.icAllCategory, name: "Danh mục")];
    categories.addAll(DataFilter.categories);

    return Container(
      alignment: Alignment.centerLeft,
      child: Wrap(
        runSpacing: 25,
        children: List.generate(
          categories.length - 1,
          (index) => GestureDetector(
            onTap: index < 1
                ? widget.onTapCategory
                : () => Navigator.pushNamed(
                      context,
                      Routers.category_product,
                      arguments: categories[index],
                    ),
            child: _buildCategoryTile(
              categoryModel: categories[index],
              isOnline: index >= 1,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildCategoryTile({CategoryModel categoryModel, bool isOnline = false}) {
    return Container(
      width: AppSize.screenWidth / 4 - 15,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isOnline
              ? WidgetImageNetwork(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  url: AppUtils.convertUrl(categoryModel.image_source),
                  fit: BoxFit.fill,
                  width: 65,
                  height: 65,
                )
              : ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  child: Image.asset(
                    categoryModel.image_source,
                    width: 65,
                    height: 65,
                    fit: BoxFit.fill,
                  ),
                ),
          const SizedBox(height: 5),
          Text(
            categoryModel.name.toUpperCase(),
            style: AppStyles.DEFAULT_SMALL_BOLD,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget _buildCarousel() {
    return FutureBuilder(
      future: _viewModel.getPromotions(),
      builder: (context, snapshot) {
        bool enabled = !snapshot.hasData;
        var data = snapshot.data;
        List<String> imgs = [];

        if (!enabled) data.forEach((x) => imgs.add(x.image_source));
        return enabled
            ? WidgetCarousel(
                images: [
                  AppImages.icLogo,
                  AppImages.icLogo,
                  AppImages.icLogo,
                ],
                heightImage: AppSize.screenWidth / 2,
                bottomPaddingDot: 0,
                circular: 0,
              )
            : WidgetCarousel(
                images: imgs,
                heightImage: AppSize.screenWidth / 2,
                isOnline: true,
                bottomPaddingDot: 0,
                circular: 0,
              );
      },
    );
  }

  _buildPopUp() {
    return Container();
    // return WidgetImageNetwork(
    //   url: DataFilter.popup.popupImage,
    //   fit: BoxFit.fill,
    // );
  }
}
