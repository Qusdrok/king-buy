import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class HomeViewModel extends BaseViewModel {
  TextEditingController searchController = TextEditingController();
  int tabSelected = 0;

  init() {}

  Future<List<PromotionModel>> initMyPromotion(int limit) async {
    return await getPromotions(
      api: AppEndpoint.GET_MY_PROMOTION,
      title: "promotions",
      limit: limit,
    );
  }

  Future<List<PromotionModel>> dataMyPromotion(int limit, int offset) async {
    return await getPromotions(
      offset: offset,
      limit: limit,
      api: AppEndpoint.GET_MY_PROMOTION,
      title: "promotions",
    );
  }

  Future<List<CategoryModel>> initCategories(int limit) async {
    return await getCategories(limit: limit);
  }

  Future<List<CategoryModel>> dataCategories(int limit, int offset) async {
    return await getCategories(
      limit: limit,
      offset: offset,
    );
  }

  setSelected(int index) {
    tabSelected = index;
    notifyListeners();
  }

  Future<List<PromotionModel>> getPromotions({
    int limit,
    int offset = 0,
    String api = AppEndpoint.GET_ALL_PROMOTION,
    String title = "news",
  }) async {
    List<PromotionModel> data = [];
    NetworkState state = await authRepository.getPromotions(
      api: api,
      title: title,
      limit: limit,
      offset: offset,
    );

    if (state.isSuccess && state.data != null) data = state.data;
    return data;
  }
}
