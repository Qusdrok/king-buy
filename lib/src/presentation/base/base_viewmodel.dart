import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:toast/toast.dart';

import '../../src.dart';

abstract class BaseViewModel extends ChangeNotifier {
  final AuthRepository authRepository = AuthRepository();

  final loadingSubject = BehaviorSubject<bool>();
  final errorSubject = BehaviorSubject<String>();

  BuildContext _context;

  BuildContext get context => _context;

  setContext(BuildContext value) => _context = value;

  void setLoading(bool loading) {
    if (loading != isLoading) loadingSubject.add(loading);
  }

  bool get isLoading => loadingSubject.value;

  void setError(String message) => errorSubject.add(message);

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  @override
  void dispose() async {
    await loadingSubject.drain();
    loadingSubject.close();
    await errorSubject.drain();
    errorSubject.close();
    super.dispose();
  }

  Future<dynamic> requestLogin() async {
    await showDialog(
      context: context,
      builder: (context) => DialogConfirm(
        keyTitle: "notification",
        actionConfirm: () {},
        content: "request_login",
        transContent: true,
      ),
    );
  }

  void unFocus({bool isHideStatus = false}) {
    FocusScope.of(context).unfocus();
    if (isHideStatus) SystemChrome.setEnabledSystemUIOverlays([]);
  }

  Future<bool> showRequestConfirm({
    @required String content,
    String keyTitle,
    bool transTitle = true,
    bool transContent = false,
    Function actionCancel,
    Function actionConfirm,
  }) async {
    return await showDialog(
      context: context,
      builder: (context) => DialogConfirm(
        keyTitle: keyTitle,
        actionCancel: actionCancel,
        transTitle: transTitle,
        transContent: transContent,
        content: content,
        actionConfirm: actionConfirm,
      ),
    );
  }

  Future<dynamic> showNotification({
    @required String keyTitle,
    bool trans = true,
    String keyAction,
    Function action,
  }) async {
    await showDialog(
      context: context,
      builder: (context) => DialogError(
        keyTitle: keyTitle,
        action: action,
        trans: trans,
        keyAction: keyAction,
      ),
    );
  }

  Future handleConfirm({String msg, Function action}) async {
    await showNotification(
      keyTitle: msg ?? "Lỗi",
      trans: false,
      action: action,
    );
  }

  Future handleConfirmBtns({String msg, Function actionConfirm}) async {
    await showRequestConfirm(
      content: msg ?? "Lỗi",
      transTitle: false,
      actionConfirm: actionConfirm,
    );
  }

  Future handleImagePicker({bool isOnline = true}) async {
    File file = await showDialog(
      context: context,
      builder: (context) => DialogMethodUpload(),
    );

    if (file != null) {
      print("Avatar URL: ${file.path}");
      accountUpdate(avatar: file);
    }
  }

  void accountUpdate({
    String name,
    String phone_number,
    String dob,
    int gender,
    String email,
    File avatar,
    int is_change_password,
    String old_password,
    String new_password,
    String new_confirm_password,
  }) async {
    String token = await AppShared.getAccessToken();
    var state = await authRepository.accountUpdate(
      token: token,
      is_change_password: 0,
      old_password: old_password,
      new_confirm_password: new_confirm_password,
      new_password: new_password,
      gender: gender,
      avatar: avatar,
      phone_number: phone_number,
      name: name,
      dob: dob,
      email: email,
    );

    if (state.isSuccess && state?.data?.user != null) {
      await AppShared.setUser(state.data.user);
      Toast.show("Update thông tin thành công", context);
      Navigator.pop(context);
    } else {
      await handleConfirm(msg: state.message);
    }
  }

  Future<List<CategoryModel>> getCategories({int limit = 5, int offset = 0}) async {
    List<CategoryModel> data = [];
    NetworkState state = await authRepository.getCategories(
      limit: limit,
      offset: offset,
    );

    if (state.isSuccess && state.data != null) data = state.data;
    return data;
  }

  Future<void> signOut() async {
    await AppFirebase.firebaseAuth.signOut();
    await AppShared.setUser(null);
    Navigator.pushReplacementNamed(context, Routers.splash);
  }
}
