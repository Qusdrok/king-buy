import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../src.dart';

class DetailProductScreen extends StatefulWidget {
  final DataModel dataModel;

  DetailProductScreen({@required this.dataModel});

  @override
  _DetailProductScreenState createState() => _DetailProductScreenState();
}

class _DetailProductScreenState extends State<DetailProductScreen> {
  YoutubePlayerController _controller;
  DetailProductViewModel _viewModel;
  ProductModel _productModel;

  int _sale;

  @override
  void initState() {
    super.initState();
    _productModel = widget.dataModel.product;
    _sale = _productModel?.sale_off ?? 0;

    if (_productModel.video_link != null)
      _controller = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(_productModel.video_link),
        flags: YoutubePlayerFlags(mute: true),
      );
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailProductViewModel>(
      viewModel: DetailProductViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                actions: [
                  WidgetActionHeader(url: AppImages.icSearchWhite),
                  const SizedBox(width: 10),
                  WidgetActionHeader(url: AppImages.icHomeWhite, height: 25),
                  const SizedBox(width: 10),
                  WidgetActionHeader(url: AppImages.icFilterWhite),
                  const SizedBox(width: 10),
                  WidgetActionHeader(url: AppImages.icShoppingCart, amount: 5),
                  const SizedBox(width: 15),
                ],
              ),
              Expanded(child: _buildBody()),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          WidgetProductPath(model: widget.dataModel),
          const SizedBox(height: 10),
          _buildProduct(),
          _buildDivider(),
          _buildGift(),
          _buildDivider(),
          _buildAddress(),
          _buildDivider(),
          _buildProductDetail(),
          _buildDivider(),
          _buildPhysicalDetail(),
          _buildDivider(),
          _buildVideo(),
          _buildDivider(),
          _buildTrademarkDetail(),
          _buildDivider(),
          _buildReviewAndComment(),
        ],
      ),
    );
  }

  Widget _buildReviewAndComment() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(
            title: "Đánh giá & Bình luận",
            padding: EdgeInsets.all(0),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "Đánh giá trung bình",
                      style: AppStyles.DEFAULT_MEDIUM,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      "(${widget.dataModel.rating.rating_count.toInt() ?? 0} đánh giá)",
                      style: AppStyles.DEFAULT_MEDIUM.copyWith(
                        color: AppColors.grey.withOpacity(0.85),
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      "${widget.dataModel.rating.avg_rating ?? 0}/5",
                      style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                        color: AppColors.grey.withOpacity(0.85),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                flex: 2,
                child: Column(
                  children: [
                    _buildProgressBar(
                      star: 5,
                      review: widget.dataModel.rating.five_star_count ?? 0,
                    ),
                    _buildProgressBar(
                      star: 4,
                      review: widget.dataModel.rating.four_star_count ?? 0,
                    ),
                    _buildProgressBar(
                      star: 3,
                      review: widget.dataModel.rating.three_star_count ?? 0,
                    ),
                    _buildProgressBar(
                      star: 2,
                      review: widget.dataModel.rating.two_star_count ?? 0,
                    ),
                    _buildProgressBar(
                      star: 1,
                      review: widget.dataModel.rating.one_star_count ?? 0,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          WidgetButton(
            title: "Viết đánh giá",
            colorBg: Colors.white,
          ),
          const SizedBox(height: 5),
          _buildDivider(
            thickness: 0.85,
            color: AppColors.black.withOpacity(0.35),
          ),
          Wrap(
            children: List.generate(
              widget.dataModel.reviews.length,
              (index) => _buildReviewTile(
                ratingModel: widget.dataModel.reviews[index],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildReviewTile({ReviewModel ratingModel, bool isShowBuy = false}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircleAvatar(
          radius: 20,
          backgroundImage: CachedNetworkImageProvider(
            ratingModel.avatar_source,
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Text(
                    ratingModel?.name ?? "",
                    style: AppStyles.DEFAULT_MEDIUM,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(width: 8),
                  if (ratingModel.is_buy == 1 && isShowBuy)
                    Expanded(
                      child: Row(
                        children: [
                          Image.asset(
                            AppImages.icCheckActive,
                            width: 12,
                            height: 12,
                            fit: BoxFit.fill,
                          ),
                          const SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              "Đã mua hàng ở King Buy",
                              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                                color: AppColors.primaryLight,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        AppUtils.convertPhoneNumber(
                          ratingModel.phone_number,
                          code: "0",
                          isCenter: false,
                          isCode: false,
                        ),
                        style: AppStyles.DEFAULT_MEDIUM.copyWith(
                          color: AppColors.grey.withOpacity(0.5),
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              WidgetRowStar(star: ratingModel.star.toDouble()),
              const SizedBox(height: 5),
              Text(
                ratingModel.comment,
                style: AppStyles.DEFAULT_MEDIUM,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
              const SizedBox(height: 10),
              Text(
                ratingModel.updated_at,
                style: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: AppColors.grey.withOpacity(0.5),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildProgressBar({int star, int review = 0}) {
    Color _color = AppColors.grey.withOpacity(0.75);
    return Row(
      children: [
        Expanded(
          child: Row(
            children: [
              Text(
                star.toString(),
                style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                  color: _color,
                ),
              ),
              const SizedBox(width: 5),
              Image.asset(
                AppImages.icStarOff,
                width: 13,
                height: 13,
                color: _color,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
        const SizedBox(width: 5),
        Expanded(
          flex: 3,
          child: Container(
            height: 13,
            decoration: BoxDecoration(
              color: AppColors.grey.withOpacity(0.35),
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            alignment: Alignment.centerLeft,
            child: Container(
              width: (review * 10).toDouble(),
              decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
            ),
          ),
        ),
        const SizedBox(width: 5),
        Expanded(
          flex: 2,
          child: Text(
            "${review} đánh giá",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: _color,
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  Widget _buildTrademarkDetail() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(title: "Thông tin thương hiệu"),
        ],
      ),
    );
  }

  Widget _buildDivider({double thickness, Color color}) => Divider(
        color: color ?? AppColors.grey.withOpacity(0.25),
        thickness: thickness ?? 8,
      );

  Widget _buildProductDetail() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(title: "Thông tin sản phẩm"),
          if (_productModel.content != null) _buildHtml(_productModel.content),
        ],
      ),
    );
  }

  Widget _buildPhysicalDetail() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(title: "Thông số kỹ thuật"),
          if (_productModel.specifications != null) _buildHtml(_productModel.specifications),
        ],
      ),
    );
  }

  Widget _buildHtml(String content) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 6),
      child: Html(
        data: content,
        style: {
          "body": Style(
            fontSize: FontSize(18.0),
            fontWeight: FontWeight.bold,
          ),
        },
      ),
    );
  }

  Widget _buildAddress() {
    var _shops = widget.dataModel.shopAddress;
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(title: "Hệ thống King Buy"),
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Wrap(
              runSpacing: 8,
              children: List.generate(
                _shops.length,
                (index) => WidgetShopAddress(
                  shopModel: _shops[index],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGift() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(
                AppImages.icGiftP,
                width: 20,
                height: 25,
                fit: BoxFit.fill,
              ),
              const SizedBox(width: 12),
              Text(
                "Quà tặng kèm:",
                style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                  color: AppColors.blue,
                ),
              ),
            ],
          ),
          const SizedBox(height: 5),
          Wrap(
            runSpacing: 20,
            children: List.generate(
              _productModel.gifts.length,
              (index) => _buildGiftItem(
                giftModel: _productModel.gifts[index],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGiftItem({GiftModel giftModel}) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 70,
          height: 72,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8)),
            border: Border.all(
              color: AppColors.black.withOpacity(0.35),
              width: 1,
            ),
          ),
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: WidgetImageNetwork(
            url: AppUtils.convertUrl(giftModel.image_source),
            fit: BoxFit.fill,
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: Container(
            height: 72,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  giftModel.name,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                WidgetTextMoney(
                  isNoExpanded: true,
                  money: giftModel.price,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildVideo() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          WidgetTitleHeader(title: "Video"),
          if (_productModel.video_link != null)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: YoutubePlayer(
                controller: _controller,
                bottomActions: [
                  CurrentPosition(),
                  ProgressBar(isExpanded: true),
                ],
              ),
            ),
        ],
      ),
    );
  }

  Widget _buildProduct() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 13, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetImageNetwork(
            url: AppUtils.convertUrl(widget.dataModel.product.image_source),
            fit: BoxFit.fill,
            width: AppSize.screenWidth,
            height: 300,
          ),
          const SizedBox(height: 8),
          Text(
            _productModel?.name ?? "Sản phẩm",
            style: AppStyles.DEFAULT_LARGE_BOLD,
          ),
          const SizedBox(height: 8),
          Row(
            children: [
              WidgetRowStar(star: _productModel.star),
              const SizedBox(width: 5),
              if ((widget.dataModel.rating.rating_count ?? 0) > 0)
                Text(
                  "(Xem ${widget.dataModel.rating.rating_count.toInt()} nhận xét)",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    color: AppColors.blue,
                  ),
                ),
            ],
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  WidgetTextMoney(
                    align: MainAxisAlignment.start,
                    money: _productModel?.sale_price ?? 600000,
                    textStyle: AppStyles.DEFAULT_LARGE.copyWith(
                      color: AppColors.red,
                    ),
                    isNoExpanded: true,
                  ),
                  const SizedBox(width: 15),
                  WidgetTextMoney(
                    align: MainAxisAlignment.start,
                    money: _productModel?.price ?? 600000,
                    textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                      decoration: TextDecoration.lineThrough,
                    ),
                    isNoExpanded: true,
                  ),
                ],
              ),
              if (_sale > 0) WidgetSale(sale: _sale)
            ],
          ),
          const SizedBox(height: 8),
          RichText(
            text: TextSpan(
              text: 'Tình trạng:',
              style: AppStyles.DEFAULT_MEDIUM.copyWith(
                color: AppColors.black,
              ),
              children: [
                TextSpan(
                  text: ' Còn hàng',
                  style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                    color: Colors.green,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 15),
          Center(
            child: WidgetButton(
              width: AppSize.screenWidth / 2,
              title: "Đặt mua trả góp",
              styleTitle: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
                color: Colors.white,
              ),
              onTap: () {},
              circular: 8,
              colorBg: AppColors.orangeLight,
              colorBorder: Colors.transparent,
            ),
          ),
        ],
      ),
    );
  }
}
