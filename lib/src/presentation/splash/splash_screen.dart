import 'package:flutter/material.dart';

import '../../src.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin {
  SplashViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget(
      viewModel: SplashViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, builder, child) {
        return Scaffold(
          body: SafeArea(
            child: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Stack(
      children: [
        Image.asset(
          AppImages.imgBgSplash,
          width: AppSize.screenWidth,
          height: AppSize.screenHeight,
          fit: BoxFit.fill,
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.only(bottom: 35),
            child: WidgetLogo(),
          ),
        ),
      ],
    );
  }
}
