import 'package:flutter/material.dart';

import '../../src.dart';

class SplashViewModel extends BaseViewModel {
  init() async {
    setLoading(true);
    DataFilter.categories = await getCategories(limit: 99);
    DataFilter.popup = await getPopup();
    DataFilter.contacts = await getContacts();
    DataFilter.config = await getConfigs();
    UserModel userModel = await AppShared.getUser();

    AppFirebase.firebaseAuth.authStateChanges().listen(
          (user) async {
        if (user != null || userModel != null) {
          Navigator.pushNamed(context, Routers.navigation);
        } else {
          Navigator.pushNamed(context, Routers.login);
        }
      },
    );

    await Future.delayed(Duration(milliseconds: 750));
    setLoading(false);
  }

  Future<PopupModel> getPopup() async {
    NetworkState state = await authRepository.getPopup();
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }

  Future<ContactsModel> getContacts() async {
    NetworkState state = await authRepository.getContacts();
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }

  Future<ConfigsAppModel> getConfigs() async {
    NetworkState state = await authRepository.getConfigs();
    if (state.isSuccess && state.data != null) return state.data;
    return null;
  }
}
