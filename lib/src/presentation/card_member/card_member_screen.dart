import 'package:barcode/barcode.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class CardMemberScreen extends StatefulWidget {
  @override
  _CardMemberScreenState createState() => _CardMemberScreenState();
}

class _CardMemberScreenState extends State<CardMemberScreen> {
  CardMemberViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<CardMemberViewModel>(
      viewModel: CardMemberViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Thẻ thành viên",
                  alignTitle: Alignment.centerLeft,
                  trans: false,
                ),
                _buildBody(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15, left: 15, right: 15),
          child: _buildBarcode(),
        ),
        const SizedBox(height: 15),
        WidgetTitleHeader(title: "COUPON CHƯA SỬ DỤNG (2)"),
        const SizedBox(height: 15),
        WidgetCoupon(),
        const SizedBox(height: 15),
        WidgetCoupon(),
        const SizedBox(height: 15),
        WidgetTitleHeader(
          title: "Xem tất cả coupon",
          style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
            color: AppColors.red,
          ),
          align: Alignment.center,
        ),
      ],
    );
  }

  Widget _buildBarcode() {
    return Container(
      width: AppSize.screenWidth,
      height: 180,
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.red,
          width: 12,
        ),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Container(
        width: AppSize.screenWidth - 24,
        height: 156,
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 25),
        child: BarcodeWidget(
          barcode: Barcode.code128(),
          data: 'Hello Flutter',
          textPadding: 15,
          width: AppSize.screenWidth - 70,
          height: 110,
        ),
      ),
    );
  }
}
