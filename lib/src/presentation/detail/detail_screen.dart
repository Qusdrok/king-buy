import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';

import '../../src.dart';

class DetailScreen extends StatefulWidget {
  final dynamic model;

  DetailScreen({@required this.model});

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  DetailViewModel _viewModel;
  String _title;

  @override
  void initState() {
    super.initState();
    _title = widget.model is CouponModel ? widget.model.name : widget.model.title;
  }

  @override
  Widget build(BuildContext context) {
    return BaseWidget<DetailViewModel>(
      viewModel: DetailViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                keyTitle: widget.model is CouponModel ? _title : "Chi tiết",
                alignTitle: Alignment.centerLeft,
                trans: false,
              ),
              Expanded(child: _buildBody()),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            _title,
            style: AppStyles.DEFAULT_MEDIUM_BOLD.copyWith(
              color: AppColors.red,
            ),
          ),
          const SizedBox(height: 10),
          widget.model is CouponModel
              ? Text(
                  widget.model.description,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                )
              : Html(
                  data: widget.model.content,
                  style: {
                    "body": Style(
                      fontSize: FontSize(18.0),
                    ),
                  },
                ),
          const SizedBox(height: 15),
          WidgetImageNetwork(
            url: AppUtils.convertUrl(widget.model.image_source),
            fit: BoxFit.fill,
            width: AppSize.screenWidth,
            height: AppSize.screenWidth / 2,
          ),
        ],
      ),
    );
  }
}
