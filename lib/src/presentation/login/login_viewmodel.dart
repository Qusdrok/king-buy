import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:toast/toast.dart';

import '../../src.dart';

class LoginViewModel extends BaseViewModel {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  GlobalKey<FormState> formSignIn = GlobalKey<FormState>();
  GlobalKey<FormState> formSignUp = GlobalKey<FormState>();

  bool obSecureText = true;
  bool isSignIn = true;

  init() {}

  switchScreen() {
    obSecureText = true;
    isSignIn = !isSignIn;
    usernameController.clear();
    passwordController.clear();
    notifyListeners();
  }

  showPassword() {
    obSecureText = !obSecureText;
    notifyListeners();
  }

  signUp() {
    unFocus();
    if (!formSignUp.currentState.validate()) return;
    handleRegister();
  }

  signIn() {
    unFocus();
    if (!formSignIn.currentState.validate()) return;
    handleLogin(
      loginType: LoginType.normal,
      account: usernameController.text,
      password: passwordController.text,
    );
  }

  Future<void> handleRegister() async {
    loadingSubject.add(true);
    String account = usernameController.text.trim();
    var state = await authRepository.register(
      account: account,
      password: passwordController.text.trim(),
      confirmPassword: confirmPasswordController.text.trim(),
    );

    await handleConfirm(msg: state.response.message);
    if (state.isSuccess && state.response.status == 1)
      Navigator.pushNamed(
        context,
        Routers.enter_otp,
        arguments: account,
      );
    loadingSubject.add(false);
  }

  void handleLogin({
    String accessToken,
    LoginType loginType,
    String account,
    String password,
  }) async {
    setLoading(true);
    NetworkState<DataModel> state = await authRepository.login(
      accessToken: accessToken,
      loginType: loginType,
      account: account,
      password: password,
    );

    try {
      setLoading(false);
      if (state.isSuccess && state.data != null) {
        state.data.user.setRewardPoint(state.data.reward_points);
        await AppShared.setUser(state.data.user);

        Toast.show("Đăng nhập thành công", context);
        Navigator.pushReplacementNamed(context, Routers.navigation);
      } else {
        handleConfirm(msg: "Đăng nhập thất bại");
      }
    } catch (e) {
      print(e.toString());
      await handleConfirm(msg: state.response.message);
      setLoading(false);
    }
  }

  void loginGoogleFirebase() async {
    try {
      setLoading(true);
      GoogleSignInAccount account = await GoogleSignIn(scopes: ['email']).signIn();
      GoogleSignInAuthentication authentication = await account.authentication;

      AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: authentication.accessToken,
        idToken: authentication.idToken,
      );

      UserCredential authResult = await AppFirebase.firebaseAuth.signInWithCredential(credential);
      User user = authResult.user;

      if (user != null) {
        final User currentUser = AppFirebase.firebaseAuth.currentUser;
        handleLogin(
          account: currentUser.email,
          loginType: LoginType.google,
          accessToken: authentication.accessToken,
        );
      } else {
        setLoading(false);
      }
    } catch (error) {
      print(error.toString());
      await handleConfirm(msg: error.toString());
      setLoading(false);
    }
  }

  void loginFacebookFirebase() async {
    setLoading(true);
    final facebookLogin = FacebookLogin();
    if (await facebookLogin.isLoggedIn) await facebookLogin.logOut();

    final result = await facebookLogin.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final credential = FacebookAuthProvider.credential(result.accessToken.token);
        final user = (await AppFirebase.firebaseAuth.signInWithCredential(credential)).user;

        handleLogin(
          loginType: LoginType.facebook,
          accessToken: result.accessToken.token,
          account: user.email,
        );
        break;
      case FacebookLoginStatus.cancelledByUser:
        await handleConfirm(msg: result.errorMessage);
        setLoading(false);
        break;
      case FacebookLoginStatus.error:
        print(result.errorMessage);
        await handleConfirm(msg: result.errorMessage);
        setLoading(false);
        break;
    }
  }

  @override
  void dispose() async {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
