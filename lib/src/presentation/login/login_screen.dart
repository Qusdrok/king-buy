import 'package:flutter/material.dart';

import '../../src.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<LoginViewModel>(
      viewModel: LoginViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: _buildBody(),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return AnimatedSwitcher(
      transitionBuilder: (child, animation) => RotationTransition(turns: animation, child: child),
      layoutBuilder: (widget, list) => Stack(children: [widget, ...list]),
      switchInCurve: Curves.easeInBack,
      switchOutCurve: Curves.easeInBack.flipped,
      duration: Duration(seconds: 1),
      child: GestureDetector(
        onTap: _viewModel.unFocus,
        child: StreamBuilder<bool>(
          stream: _viewModel.loadingSubject,
          builder: (context, snapshot) {
            bool loading = snapshot.data ?? false;
            return SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  WidgetLogo(width: AppSize.screenWidth / 4, height: AppSize.screenWidth / 4),
                  _viewModel.isSignIn ? _buildSignIn(loading) : _buildSignUp(loading),
                  SizedBox(height: AppSize.screenWidth / 2),
                  _buildSocialButtons(
                    loading: loading,
                    title: "${_viewModel.isSignIn ? "Không" : "Đã"} có tài khoản ?",
                    titleUnderline: "Đăng ${_viewModel.isSignIn ? "ký" : "nhập"}",
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildSignIn(bool loading) {
    return Column(
      children: [
        const SizedBox(height: 10),
        Text(
          "ĐĂNG NHẬP",
          style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
            fontSize: 25,
          ),
        ),
        const SizedBox(height: 20),
        Form(
          key: _viewModel.formSignIn,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              WidgetTextField(
                controller: _viewModel.usernameController,
                readOnly: loading,
                validator: AppValid.validateEmailOrPhoneNumber(context),
                hintText: "Email hoặc số điện thoại",
                prefixImg: AppImages.icProfile2,
              ),
              const SizedBox(height: 10),
              WidgetTextField(
                controller: _viewModel.passwordController,
                readOnly: loading,
                obscureText: true,
                validator: AppValid.validatePassword(context),
                hintText: "Mật khẩu",
                prefixImg: AppImages.icLock2,
                paddingImg: EdgeInsets.only(left: 24, right: 13, top: 10, bottom: 10),
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () => Navigator.pushNamed(context, Routers.forgot_password),
                    child: Text(
                      "Quên mật khẩu ?",
                      style: AppStyles.DEFAULT_MEDIUM,
                    ),
                  ),
                  const SizedBox(width: 15),
                  WidgetButtonGradientAnim(
                    width: AppSize.screenWidth / 3,
                    title: "Đăng nhập",
                    trans: false,
                    textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                      color: Colors.white,
                    ),
                    loading: loading,
                    height: 50,
                    action: () => _viewModel.signIn(),
                    colorStart: AppColors.red,
                    colorEnd: AppColors.red,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSignUp(bool loading) {
    return Column(
      children: [
        const SizedBox(height: 10),
        Text(
          "ĐĂNG KÝ TÀI KHOẢN",
          style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
            fontSize: 25,
          ),
        ),
        const SizedBox(height: 20),
        Form(
          key: _viewModel.formSignUp,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              WidgetTextField(
                controller: _viewModel.usernameController,
                readOnly: loading,
                validator: AppValid.validateEmailOrPhoneNumber(context),
                hintText: "Email hoặc số điện thoại",
                prefixImg: AppImages.icProfile2,
              ),
              const SizedBox(height: 10),
              WidgetTextField(
                controller: _viewModel.passwordController,
                readOnly: loading,
                obscureText: true,
                validator: AppValid.validatePassword(context),
                hintText: "Mật khẩu",
                prefixImg: AppImages.icLock2,
                paddingImg: EdgeInsets.only(left: 24, right: 13, top: 10, bottom: 10),
              ),
              const SizedBox(height: 10),
              WidgetTextField(
                controller: _viewModel.confirmPasswordController,
                readOnly: loading,
                obscureText: true,
                validator: AppValid.validateConfirmPassword(
                  context,
                  _viewModel.passwordController.text,
                ),
                hintText: "Nhập lại mật khẩu",
                prefixImg: AppImages.icLock2,
                paddingImg: EdgeInsets.only(left: 24, right: 13, top: 10, bottom: 10),
              ),
              const SizedBox(height: 10),
              WidgetButtonGradientAnim(
                width: AppSize.screenWidth / 3,
                title: "Đăng ký",
                trans: false,
                height: 50,
                textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
                  color: Colors.white,
                ),
                loading: loading,
                action: () => _viewModel.signUp(),
                colorStart: AppColors.red,
                colorEnd: AppColors.red,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildSocialButtons({String title, String titleUnderline, bool loading}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "Hoặc đăng nhập bằng",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.black,
            ),
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildIconLogin(
                url: AppImages.icFacebook,
                onTap: () => _viewModel.loginFacebookFirebase(),
                loading: loading,
              ),
              const SizedBox(width: 12),
              _buildIconLogin(
                url: AppImages.icGoogle,
                onTap: () => _viewModel.loginGoogleFirebase(),
                loading: loading,
              ),
            ],
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title ?? "Không có tài khoản ?",
                style: AppStyles.DEFAULT_MEDIUM,
              ),
              const SizedBox(width: 5),
              GestureDetector(
                onTap: !loading ? () => _viewModel.switchScreen() : null,
                child: Text(
                  titleUnderline ?? "Đăng ký",
                  style: AppStyles.DEFAULT_MEDIUM.copyWith(
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildIconLogin({String url, Function onTap, bool loading}) {
    return InkWell(
      onTap: loading ? null : onTap,
      child: Container(
        width: 35,
        height: 35,
        child: Image.asset(
          url,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
