import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import '../../src.dart';

class AccountUpdateViewModel extends BaseViewModel {
  TextEditingController usernameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController dateTimeController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  GlobalKey<FormState> formUpdate = GlobalKey<FormState>();

  int gender = 0;
  String avatar;

  init() {}

  updateAvatar() async => avatar = await handleImagePicker();

  initText(UserModel userModel) {
    usernameController.text = userModel.name;
    phoneController.text = userModel.phone_number;
    dateTimeController.text = userModel.date_of_birth;
    emailController.text = userModel.email;
  }

  setGender(int index) {
    gender = index;
    notifyListeners();
  }
}
