import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/presentation/presentation.dart';

import '../../src.dart';

class AccountUpdateScreen extends StatefulWidget {
  @override
  _AccountUpdateScreenState createState() => _AccountUpdateScreenState();
}

class _AccountUpdateScreenState extends State<AccountUpdateScreen> {
  AccountUpdateViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountUpdateViewModel>(
      viewModel: AccountUpdateViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                keyTitle: "Thông tin cá nhân",
                alignTitle: Alignment.centerLeft,
                trans: false,
                actions: [
                  _buildSave(),
                ],
              ),
              Expanded(
                child: StreamBuilder(
                  stream: AppShared.watchUser(),
                  builder: (context, snapshot) {
                    bool enabled = !snapshot.hasData;
                    UserModel data = snapshot.data;

                    if (!enabled) _viewModel.initText(data);
                    return enabled ? WidgetShimmer(child: _buildBody(null, true)) : _buildBody(data, false);
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  Widget _buildSave() {
    return Padding(
      padding: const EdgeInsets.only(right: 15, top: 6, bottom: 6),
      child: GestureDetector(
        onTap: () => _viewModel.accountUpdate(
          is_change_password: 0,
          email: _viewModel.emailController.text.trim(),
          phone_number: _viewModel.phoneController.text.trim(),
          name: _viewModel.usernameController.text.trim(),
          gender: _viewModel.gender,
          dob: _viewModel.dateTimeController.text.trim(),
        ),
        child: Container(
          width: 75,
          height: 40,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: AppColors.red,
          ),
          alignment: Alignment.center,
          child: Text(
            "Lưu",
            style: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody(UserModel user, bool loading) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(12),
      child: Column(
        children: [
          const SizedBox(height: 25),
          _buildAvatar(imgUrl: user?.avatar_source),
          const SizedBox(height: 45),
          _buildTextField(loading),
          const SizedBox(height: 20),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Giới tính",
              style: AppStyles.DEFAULT_MEDIUM,
              textAlign: TextAlign.start,
            ),
          ),
          Container(
            width: AppSize.screenWidth,
            child: Row(
              children: [
                _buildRadio(value: 0, title: "Nam"),
                _buildRadio(value: 1, title: "Nữ"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRadio({int value, String title}) {
    return Row(
      children: [
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: AppColors.red,
          ),
          child: Radio(
            value: value,
            groupValue: _viewModel.gender,
            activeColor: AppColors.red,
            focusColor: AppColors.red,
            onChanged: (index) => _viewModel.setGender(index),
          ),
        ),
        Text(
          title,
          style: AppStyles.DEFAULT_MEDIUM,
        ),
      ],
    );
  }

  Widget _buildTextField(bool loading) {
    return Form(
      key: _viewModel.formUpdate,
      child: Column(
        children: [
          WidgetTextField(
            controller: _viewModel.usernameController,
            readOnly: loading,
            hintText: "Họ và tên",
            prefixImg: AppImages.icProfile2,
          ),
          const SizedBox(height: 10),
          WidgetTextField(
            controller: _viewModel.phoneController,
            readOnly: loading,
            hintText: "Số điện thoại",
            prefixImg: AppImages.icCall2,
          ),
          const SizedBox(height: 10),
          WidgetTextField(
            controller: _viewModel.dateTimeController,
            readOnly: loading,
            hintText: "Ngày tháng năm sinh",
            prefixImg: AppImages.icCalendar,
          ),
          const SizedBox(height: 10),
          WidgetTextField(
            controller: _viewModel.emailController,
            readOnly: loading,
            hintText: "Email",
            paddingImg: EdgeInsets.only(left: 25, right: 13, top: 16, bottom: 16),
            prefixImg: AppImages.icEmail,
          ),
        ],
      ),
    );
  }

  Widget _buildAvatar({String imgUrl}) {
    return Stack(
      overflow: Overflow.visible,
      children: [
        WidgetImageNetwork(
          url: AppUtils.convertUrl(imgUrl ?? "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg"),
          fit: BoxFit.fill,
          width: 120,
          height: 120,
          shape: ImageNetworkShape.circle,
        ),
        Positioned(
          bottom: -2,
          right: 2,
          child: GestureDetector(
            onTap: () => _viewModel.handleImagePicker(),
            child: CircleAvatar(
              radius: 15,
              backgroundColor: Colors.white,
              child: Image.asset(
                AppImages.icPencil,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
