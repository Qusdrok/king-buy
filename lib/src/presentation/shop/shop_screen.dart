import 'package:flutter/material.dart';

import '../../src.dart';

class ShopScreen extends StatefulWidget {


  @override
  _ShopScreenState createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  ShopViewModel _viewModel;
  List<Widget> _tabViews = [];
  List<Tab> _tabs = [];

  @override
  void initState() {
    super.initState();
    DataFilter.categories.forEach((x) {
      _tabs.add(Tab(text: x.name));
      _tabViews.add(
        WidgetProductItems(
          api: AppEndpoint.GET_PRODUCT_BY_CATEGORY,
          title: "products",
          productId: x.id,
          axis: Axis.vertical,
          padding: EdgeInsets.only(top: 15, left: 5, right: 5),
          width: AppSize.screenWidth / 2 - 15,
        ),
      );
    });
    _tabController = TabController(length: _tabs.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<ShopViewModel>(
      viewModel: ShopViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                keyTitle: "Cửa hàng",
                alignTitle: Alignment.centerLeft,
                trans: false,
                actions: [
                  WidgetActionHeader(url: AppImages.icFilterWhite),
                  const SizedBox(width: 15),
                  WidgetActionHeader(url: AppImages.icShoppingCart),
                  const SizedBox(width: 10),
                ],
              ),
              Expanded(child: _buildBody()),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        WidgetTabBarDecorated(
          width: 3,
          color: AppColors.grey,
          tabBar: TabBar(
            controller: _tabController,
            isScrollable: true,
            indicatorColor: AppColors.red,
            indicatorWeight: 3,
            labelStyle: AppStyles.DEFAULT_MEDIUM_BOLD,
            tabs: _tabs,
          ),
        ),
        Expanded(
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: _tabViews,
          ),
        ),
      ],
    );
  }
}
