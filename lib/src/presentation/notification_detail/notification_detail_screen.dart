import 'package:flutter/material.dart';

import '../../src.dart';

class NotificationDetailScreen extends StatefulWidget {
  final NotificationModel model;

  NotificationDetailScreen({this.model});

  @override
  _NotificationDetailScreenState createState() => _NotificationDetailScreenState();
}

class _NotificationDetailScreenState extends State<NotificationDetailScreen> {
  NotificationDetailViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<NotificationDetailViewModel>(
      viewModel: NotificationDetailViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Chi Tiết Thông Báo",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Container();
  }
}
