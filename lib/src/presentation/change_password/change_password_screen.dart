import 'package:flutter/material.dart';

import '../../src.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  ChangePasswordViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ChangePasswordViewModel>(
      viewModel: ChangePasswordViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white,
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Đổi mật khẩu",
                  trans: false,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _buildTextField(
            controller: _viewModel.oldPasswordController,
            hintText: "Mật khẩu cũ",
          ),
          const SizedBox(height: 8),
          _buildTextField(
            controller: _viewModel.newPasswordController,
            hintText: "Mật khẩu mới",
          ),
          const SizedBox(height: 8),
          _buildTextField(
            controller: _viewModel.confirmNewPasswordController,
            hintText: "Nhập lại mật khẩu mới",
          ),
          Spacer(),
          WidgetButtonGradientAnim(
            width: AppSize.screenWidth,
            height: 50,
            title: "Đồng ý",
            trans: false,
            colorStart: AppColors.primaryDark,
            colorEnd: AppColors.primaryDark,
            action: () => _viewModel.handleConfirm(msg: "Bạn chắc chắn thay đổi mật khẩu?"),
            textStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
              color: AppColors.white,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTextField({
    TextEditingController controller,
    String hintText,
    TextInputType inputType,
    validator,
    bool readOnly = false,
  }) {
    return TextFormField(
      controller: controller,
      readOnly: readOnly,
      obscureText: _viewModel.obSecureText,
      validator: validator,
      keyboardType: inputType ?? TextInputType.text,
      decoration: InputDecoration(
        suffixIcon: Padding(
          padding: const EdgeInsets.only(right: 12),
          child: IconButton(
            icon: Icon(
              _viewModel.obSecureText ? Icons.remove_red_eye_outlined : Icons.remove_red_eye,
              color: AppColors.grey,
            ),
            color: Colors.black,
            onPressed: () => _viewModel.showPassword(),
          ),
        ),
        focusedBorder: _buildBorderTextField(AppColors.primaryLight),
        enabledBorder: _buildBorderTextField(AppColors.primaryLight),
        errorBorder: _buildBorderTextField(AppColors.red),
        focusedErrorBorder: _buildBorderTextField(AppColors.primaryLight),
        contentPadding: EdgeInsets.fromLTRB(25.0, 16.0, 25.0, 16.0),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: AppStyles.DEFAULT_MEDIUM.copyWith(
          color: AppColors.grey,
          fontSize: 16,
        ),
      ),
    );
  }

  OutlineInputBorder _buildBorderTextField(Color color) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(150),
      borderSide: BorderSide(
        color: color,
        width: 1.2,
      ),
    );
  }
}
