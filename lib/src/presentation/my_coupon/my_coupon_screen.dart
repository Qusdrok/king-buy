import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/presentation/widgets/widget_tab_bar.dart';

import '../../src.dart';

class MyCouponScreen extends StatefulWidget {
  @override
  _MyCouponScreenState createState() => _MyCouponScreenState();
}

class _MyCouponScreenState extends State<MyCouponScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  MyCouponViewModel _viewModel;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<MyCouponViewModel>(
      viewModel: MyCouponViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Column(
            children: [
              WidgetAppBar(
                keyTitle: "Coupon của tôi",
                alignTitle: Alignment.centerLeft,
                trans: false,
              ),
              Expanded(child: _buildBody()),
            ],
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Column(
      children: [
        WidgetTabBar(
          tabs: [
            "Chưa sử dụng",
            "Đã sử dụng",
            "Hết hạn",
          ],
          onTap: _viewModel.setSelected,
        ),
        Expanded(
          child: ListView.separated(
            padding: EdgeInsets.only(top: 15, bottom: 15),
            physics: BouncingScrollPhysics(),
            separatorBuilder: (context, index) => Container(height: 15),
            itemBuilder: (context, index) => WidgetCoupon(),
            itemCount: 10,
          ),
        ),
      ],
    );
  }
}
