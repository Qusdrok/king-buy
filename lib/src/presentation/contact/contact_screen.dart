import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/configs/configs.dart';

import '../../src.dart';

class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen> {
  ContactViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<ContactViewModel>(
      viewModel: ContactViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  keyTitle: "Liên hệ",
                  trans: false,
                  alignTitle: Alignment.centerLeft,
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 30),
            child: Column(
              children: [
                WidgetRowIcon(
                  title: DataFilter.contacts.message,
                  icon: Image.asset(
                    AppImages.icFacebookChat,
                    width: 25,
                    height: 26,
                    fit: BoxFit.fill,
                  ),
                  imgUrl: AppImages.icChatBubbles,
                ),
                const SizedBox(height: 25),
                WidgetRowIcon(
                  title: DataFilter.contacts.hotLine,
                  icon: Image.asset(
                    AppImages.icEPath,
                    width: 8,
                    height: 12,
                    fit: BoxFit.fill,
                  ),
                  imgUrl: AppImages.icCall,
                ),
                const SizedBox(height: 25),
                WidgetRowIcon(
                  title: DataFilter.contacts.email,
                  height: 20,
                  icon: Image.asset(
                    AppImages.icEPath,
                    width: 8,
                    height: 12,
                    fit: BoxFit.fill,
                  ),
                  imgUrl: AppImages.icEmailOn,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
