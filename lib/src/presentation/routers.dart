import 'package:flutter/material.dart';
import 'package:flutter_app_king_buy/src/presentation/account/account.dart';

import '../src.dart';

class Routers {
  static const String account = "/account";
  static const String account_update = "/account_update";
  static const String card_member = "/card_member";
  static const String change_password = "/change_password";
  static const String commitment = "/commitment";
  static const String contact = "/contact";
  static const String detail = "/detail";
  static const String detail_product = "/detail_product";
  static const String enter_otp = "/enter_otp";
  static const String forgot_password = "/forgot_password";
  static const String login = "/login";
  static const String my_coupon = "/my_coupon";
  static const String navigation = "/navigation";
  static const String notification = "/notification";
  static const String notification_detail = "/notification_detail";
  static const String category = "/category";
  static const String category_product = "/product_category";
  static const String search = "/search";
  static const String shop = "/shop";
  static const String splash = "/splash";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    var arguments = settings.arguments;
    switch (settings.name) {
      case account:
        return animRoute(AccountScreen());
      case card_member:
        return animRoute(CardMemberScreen());
      case change_password:
        return animRoute(ChangePasswordScreen());
      case commitment:
        return animRoute(CommitmentScreen());
      case contact:
        return animRoute(ContactScreen());
      case detail:
        return animRoute(DetailScreen(model: arguments));
      case detail_product:
        return animRoute(DetailProductScreen(dataModel: arguments));
      case enter_otp:
        return animRoute(EnterOTPScreen(account: arguments));
      case forgot_password:
        return animRoute(ForgotPasswordScreen());
      case account_update:
        return animRoute(AccountUpdateScreen());
      case login:
        return animRoute(LoginScreen());
      case my_coupon:
        return animRoute(MyCouponScreen());
      case navigation:
        return animRoute(NavigationScreen());
      case notification:
        return animRoute(NotificationScreen());
      case notification_detail:
        return animRoute(NotificationDetailScreen(model: arguments));
      case category:
        return animRoute(CategoryScreen());
      case category_product:
        return animRoute(CategoryProductScreen(model: arguments));
      case search:
        return animRoute(SearchScreen());
      case shop:
        return animRoute(ShopScreen());
      case splash:
        return animRoute(SplashScreen());
      default:
        return animRoute(
          Container(
            child: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }

  static Route animRoute(Widget page, {Offset beginOffset, String name, Object arguments}) {
    return PageRouteBuilder(
      settings: RouteSettings(name: name, arguments: arguments),
      pageBuilder: (context, animation, secondaryAnimation) => page,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = beginOffset ?? Offset(1.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin: begin, end: end).chain(
          CurveTween(curve: curve),
        );

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  static Route scaleRoute(Widget page) {
    return PageRouteBuilder(
      transitionDuration: Duration(milliseconds: 500),
      pageBuilder: (context, animation, secondAnimation) => page,
      transitionsBuilder: (context, animation, secondAnimation, child) {
        return ScaleTransition(
          scale: animation,
          alignment: Alignment.center,
          child: child,
        );
      },
    );
  }

  static Offset _center = Offset(0.0, 0.0);
  static Offset _top = Offset(0.0, 1.0);
  static Offset _bottom = Offset(0.0, -1.0);
  static Offset _left = Offset(-1.0, 0.0);
  static Offset _right = Offset(1.0, 0.0);
}
