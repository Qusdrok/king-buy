import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../src.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  AccountViewModel _viewModel;

  @override
  Widget build(BuildContext context) {
    return BaseWidget<AccountViewModel>(
      viewModel: AccountViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: SafeArea(child: _buildBody()),
        );
      },
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _buildUser(),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEDiscount,
            title: "Khuyến mãi",
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEMoto,
            height: 35,
            title: "Địa chỉ giao hàng",
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEPathCompound,
            title: "Coupon của tôi",
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEReset,
            title: "Lịch sử đặt hàng",
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEViewed,
            title: "Sản phẩm đã xem",
            height: 18,
            width: 25,
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icLock,
            title: "Đổi mật khẩu",
            height: 30,
            width: 20,
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEAboutUs,
            title: "Cam kết của KingBuy",
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEPhone,
            title: "Liên hệ",
            onTap: () => Navigator.pushNamed(context, Routers.contact),
          ),
          const SizedBox(height: 25),
          WidgetRowIcon(
            imgUrl: AppImages.icEDocument,
            title: "Điều khoản sử dụng",
            height: 25,
            width: 20,
          ),
          const SizedBox(height: 25),
          Text(
            "Hotline: ${AppUtils.convertPhoneNumber(DataFilter.config.hot_line, isHide: false)}",
            style: AppStyles.DEFAULT_LARGE,
          ),
          const SizedBox(height: 5),
          Text(
            DataFilter.config.ver_app,
            style: AppStyles.DEFAULT_MEDIUM,
          ),
          const SizedBox(height: 35),
          GestureDetector(
            onTap: () => _viewModel.signOut(),
            child: Text(
              "Đăng xuất",
              style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                color: AppColors.red,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUser() {
    return StreamBuilder(
      stream: AppShared.watchUser(),
      builder: (context, snapshot) {
        bool enabled = !snapshot.hasData;
        UserModel data = snapshot.data;

        return Column(
          children: [
            const SizedBox(height: 30),
            enabled
                ? WidgetShimmer(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _buildAvatar(null),
                        const SizedBox(height: 20),
                        Text(
                          "Trương Thị Huyền",
                          style: AppStyles.DEFAULT_LARGE_BOLD,
                        ),
                      ],
                    ),
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _buildAvatar(data),
                      const SizedBox(height: 20),
                      Text(
                        data.name,
                        style: AppStyles.DEFAULT_LARGE_BOLD,
                      ),
                      const SizedBox(height: 5),
                      Text(
                        AppUtils.convertPhoneNumber(data.phone_number ?? "0962847950", isHide: false),
                        style: AppStyles.DEFAULT_LARGE,
                      ),
                      const SizedBox(height: 5),
                      Text(
                        "${data.reward_points} điểm",
                        style: AppStyles.DEFAULT_LARGE_BOLD.copyWith(
                          color: AppColors.red,
                        ),
                      ),
                      const SizedBox(height: 35),
                    ],
                  ),
            WidgetRowIcon(
              imgUrl: AppImages.icEGroup,
              title: "Thông tin cá nhân",
              onTap: () => Navigator.pushNamed(context, Routers.account_update),
            ),
          ],
        );
      },
    );
  }

  Widget _buildAvatar(UserModel userModel) {
    return PhysicalModel(
      borderRadius: BorderRadius.all(Radius.circular(130)),
      color: Colors.transparent,
      elevation: 3,
      child: Stack(
        overflow: Overflow.visible,
        children: [
          CircleAvatar(
            radius: 70,
            backgroundImage: CachedNetworkImageProvider(
              AppUtils.convertUrl(userModel?.avatar_source) ??
                  "https://cdn.pixabay.com/photo/2021/02/12/09/36/sunflowers-6007847__340.jpg",
            ),
          ),
          Positioned(
            bottom: -2,
            right: 0,
            child: GestureDetector(
              onTap: () => _viewModel.handleImagePicker(),
              child: Image.asset(
                AppImages.icPencil,
                width: 35,
                height: 35,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
