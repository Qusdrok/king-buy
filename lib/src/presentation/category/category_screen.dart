import 'package:flutter/material.dart';

import '../../src.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> with SingleTickerProviderStateMixin {
  List<CategoryModel> _categories = [CategoryModel(image_source: AppImages.icStarOn)];
  List<Widget> _tabViews = [];
  CategoryViewModel _viewModel;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _categories.addAll(DataFilter.categories);
    _tabController = new TabController(length: _categories.length, vsync: this);

    for (int i = 0; i < _categories.length; i++)
      _tabViews.add(i == 0 ? _buildHotCategories() : _buildTabView(categoryModel: _categories[i]));
  }

  @override
  Widget build(BuildContext context) {
    AppSize.init(context);
    return BaseWidget<CategoryViewModel>(
      viewModel: CategoryViewModel(),
      onViewModelReady: (viewModel) => _viewModel = viewModel..init(),
      builder: (context, viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.white.withOpacity(0.25),
          body: SafeArea(
            child: Column(
              children: [
                WidgetAppBar(
                  leading: SizedBox(width: 15),
                  keyTitle: "Danh mục",
                  trans: false,
                  isBack: false,
                  height: 10,
                  alignTitle: Alignment.centerLeft,
                  actions: [
                    IconButton(
                      icon: Icon(
                        Icons.more_horiz,
                        color: AppColors.white,
                      ),
                      onPressed: () {},
                    ),
                  ],
                ),
                Expanded(child: _buildBody()),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildBody() {
    return Row(
      children: <Widget>[
        Container(
          width: 120,
          height: AppSize.screenHeight,
          child: ListView.separated(
            itemBuilder: (context, index) => _buildTab(
              imgUrl: _categories[index].image_source,
              title: _categories[index].name,
              isOnline: index > 0,
              index: index,
            ),
            separatorBuilder: (context, index) => Container(height: 1.5),
            itemCount: _categories?.length,
          ),
        ),
        const SizedBox(width: 3),
        Expanded(
          child: TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: _tabViews,
          ),
        ),
      ],
    );
  }

  Widget _buildHotCategories() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 7),
      child: Container(
        padding: EdgeInsets.all(12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Danh mục đang hot:",
              style: AppStyles.DEFAULT_MEDIUM_BOLD,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Wrap(
                    spacing: 10,
                    runSpacing: 20,
                    children: List.generate(
                      _categories.length,
                      (index) => _categories[index].name == null
                          ? Container()
                          : Container(
                              width: (AppSize.screenWidth - 90) / 3 - 35,
                              alignment: Alignment.center,
                              child: Column(
                                children: [
                                  WidgetImageNetwork(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    url: AppUtils.convertUrl(_categories[index].image_source),
                                    fit: BoxFit.fill,
                                    width: 65,
                                    height: 65,
                                  ),
                                  const SizedBox(height: 8),
                                  Text(
                                    _categories[index].name.toUpperCase(),
                                    style: AppStyles.DEFAULT_SMALL_BOLD,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTabView({CategoryModel categoryModel}) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: 15),
      child: Column(
        children: [
          Container(
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: AppColors.black.withOpacity(0.25),
                  blurRadius: 1.2,
                  spreadRadius: 1.2,
                ),
              ],
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  categoryModel.name,
                  style: AppStyles.DEFAULT_MEDIUM_BOLD,
                ),
                Image.asset(
                  AppImages.icChevron,
                  width: 10,
                  height: 15,
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5, right: 5, bottom: 5),
            child: Column(
              children: [
                WidgetImageNetwork(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  url: AppUtils.convertUrl(categoryModel.image_source),
                  width: AppSize.screenWidth - 70,
                  height: AppSize.screenWidth / 2,
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 3, right: 8),
            child: Container(
              width: AppSize.screenWidth - 70,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
              alignment: Alignment.center,
              child: Wrap(
                spacing: 10,
                runSpacing: 10,
                children: List.generate(
                  categoryModel.children.length,
                  (index) => _buildItem(
                    title: categoryModel.children[index].name,
                    imgUrl: categoryModel.children[index].image_source,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildItem({String imgUrl, String title}) {
    return Container(
      width: (AppSize.screenWidth - 90) / 3 - 25,
      height: 120,
      child: Column(
        children: [
          WidgetImageNetwork(
            url: AppUtils.convertUrl(imgUrl),
            height: 65,
            fit: BoxFit.fill,
          ),
          const SizedBox(height: 10),
          Expanded(
            child: Text(
              title,
              style: AppStyles.DEFAULT_MEDIUM,
              textAlign: TextAlign.center,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTab({String imgUrl, String title, bool isOnline, int index}) {
    return GestureDetector(
      onTap: () => _viewModel.setTabSelected(_tabController, index),
      child: Container(
        color: Colors.white,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned.fill(
              child: RotatedBox(
                quarterTurns: 1,
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: _viewModel.tabSelected == index ? AppColors.blue : Colors.transparent,
                        width: 3,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Column(
                children: [
                  isOnline
                      ? WidgetImageNetwork(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          url: AppUtils.convertUrl(imgUrl),
                          fit: BoxFit.fill,
                          width: 65,
                          height: 65,
                        )
                      : Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            color: AppColors.yellow.withOpacity(0.25),
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: Image.asset(
                              imgUrl,
                              width: 65,
                              height: 65,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                  const SizedBox(height: 8),
                  if (index > 0)
                    Text(
                      title.toUpperCase(),
                      style: AppStyles.DEFAULT_SMALL_BOLD,
                      textAlign: TextAlign.center,
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
