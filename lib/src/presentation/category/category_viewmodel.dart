import 'package:flutter/material.dart';

import '../../src.dart';

class CategoryViewModel extends BaseViewModel {
  int tabSelected = 0;

  init() {}

  setTabSelected(TabController controller, int index) {
    controller.animateTo(
      index,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );

    tabSelected = index;
    notifyListeners();
  }
}
