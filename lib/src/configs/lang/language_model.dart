import 'package:flutter/material.dart';

class LanguageModel {
  LanguageModel({this.locale, this.code, this.language});

  Locale locale;
  String code;
  String language;

  LanguageModel.fromJson(dynamic json) {
    locale = Locale(json['locale'][''], 'VN');
  }
}
