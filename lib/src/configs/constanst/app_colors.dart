import 'package:flutter/material.dart';

import '../../src.dart';

class AppColors {
  AppColors._();

  static final Color primary = Colors.red;
  static final Color primaryDark = Colors.red;
  static final Color primaryLight = Colors.red.withOpacity(0.5);

  static final Color grey = HexColor.fromHex("#A1A1A1");
  static final Color greyLight = HexColor.fromHex("#D5DFD0");
  static final Color greyLight2 = HexColor.fromHex("#F4F9F2");

  static final Color white = HexColor.fromHex("#EFEFEF");
  static final Color white2 = HexColor.fromHex("#E8E8E8");
  static final Color white3 = HexColor.fromHex("#DEDEDE");

  static final Color orangeLight = HexColor.fromHex("#FEA947");
  static final Color yellow = HexColor.fromHex("#FFE600");

  static final Color black = HexColor.fromHex("#212121");
  static final Color red = Colors.red;

  static final Color blue = Colors.blue.shade400;
}
