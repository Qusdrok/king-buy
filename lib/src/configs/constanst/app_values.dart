class AppValues {
  AppValues._();

  static const double INPUT_FORM_HEIGHT = 55;
  static const double HEIGHT_APP_BAR = 50;
  static const double PI = 3.14;
}
