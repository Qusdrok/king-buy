class AppImages {
  AppImages._();

  // Logo
  static final String icLogo = 'assets/logo/logo.png';

  // Background
  static final String imgBgDrawer = "assets/bgs/bg_drawer.png";
  static final String imgBgDrawerItem = "assets/bgs/bg_drawer_item.png";
  static final String imgBgDrawerItemSelected = "assets/bgs/bg_drawer_item_selected.png";
  static final String imgBgShopAddress = "assets/bgs/bg_shopadress.png";
  static final String imgBgSplash = "assets/bgs/bg_splash.png";

  // Gifs
  static final String imgGifLoader = "assets/gifs/loader.gif";
  static final String imgGifPreloader = "assets/gifs/preloader.gif";

  // Icon
  static final String icAddition = "assets/icons/addition.png";
  static final String icAvatarNotify = "assets/icons/avatar_notify.png";
  static final String icAllCategory = "assets/icons/all_category.png";
  static final String icCalendar = "assets/icons/calendar.png";
  static final String icClock = "assets/icons/clock.png";
  static final String icChevron = "assets/icons/chevron.png";
  static final String icDocument = "assets/icons/document.png";
  static final String icExit = "assets/icons/exit.png";
  static final String icGps = "assets/icons/gps.png";
  static final String icAttach = "assets/icons/ic_attach.png";
  static final String icBell = "assets/icons/ic_bell.png";
  static final String icCoin = "assets/icons/ic_coin.png";
  static final String icCreateMsg = "assets/icons/ic_create_msg.png";
  static final String icForum = "assets/icons/ic_forum.png";
  static final String icGalleryGreen = "assets/icons/ic_gallery_green.png";
  static final String icGame = "assets/icons/ic_game.png";
  static final String icHome = "assets/icons/ic_home.png";
  static final String icPack = "assets/icons/ic_pack.png";
  static final String icPlay = "assets/icons/ic_play.png";
  static final String icPhone = "assets/icons/ic_phone.png";
  static final String icSaveButton = "assets/icons/ic_save_button.png";
  static final String icCoupon = "assets/icons/ic_coupon.png";
  static final String icPoint = "assets/icons/ic_point.png";
  static final String icWallet = "assets/icons/ic_wallet.png";
  static final String icPencil = "assets/icons/pencil.png";
  static final String icPin = "assets/icons/pin.png";
  static final String icPlaceHolder = "assets/icons/placeholder.png";
  static final String icQuestionUser = "assets/icons/question_user.png";
  static final String icUser = "assets/icons/user.png";

  static final String icShoppingCart = "assets/icons/shopping-cart.png";
  static final String icShoppingCart2 = "assets/icons/shopping-cart-black.png";

  static final String icStarOff = "assets/icons/star_off.png";
  static final String icStarOn = "assets/icons/star_on.png";
  static final String icStarOn2 = "assets/icons/ic_star_on.png";
  static final String icStarOff2 = "assets/icons/ic_star_off.png";

  static final String icSend = "assets/icons/ic_send.png";
  static final String icSend2 = "assets/icons/send.png";

  static final String icRadioGroupSelected = "assets/icons/radio_group_selected.png";
  static final String icRadioGroupUnselected = "assets/icons/radio_group_unselected.png";
  static final String icRadioSelected = "assets/icons/radio_selected.png";
  static final String icRadioUnselected = "assets/icons/radio_unselected.png";

  static final String icMenuOff = "assets/icons/menu_off.png";
  static final String icMenuOn = "assets/icons/menu_on.png";

  static final String icCall = "assets/icons/call.png";
  static final String icCall2 = "assets/icons/call2.png";
  static final String icCall3 = "assets/icons/call3.png";

  static final String icProfile = "assets/icons/ic_profile.png";
  static final String icProfile2 = "assets/icons/profile.png";

  static final String icSearch = "assets/icons/ic_search.png";
  static final String icSearchWhite = "assets/icons/ic_search_white.png";

  static final String icLock2 = "assets/icons/closed_lock.png";
  static final String icLock = "assets/icons/lock.png";

  static final String icLeftArrow = "assets/icons/left_arrow.png";
  static final String icLeftArrowWhite = "assets/icons/left_arrow_white.png";

  static final String icRefreshActive = "assets/icons/ic_refresh_active.png";
  static final String icRefreshDisable = "assets/icons/ic_refresh_disable.png";

  static final String icCopyActive = "assets/icons/ic_copy_active.png";
  static final String icCheckActive = "assets/icons/ic_check_active.png";
  static final String icCheckButton = "assets/icons/ic_check_button.png";

  static final String icEAboutUs = "assets/icons/e-about-us.png";
  static final String icEDiscount = "assets/icons/e-discount.png";
  static final String icEGroup = "assets/icons/e-group.png";
  static final String icEMoto = "assets/icons/e-moto.png";
  static final String icEDocument = "assets/icons/e-document.png";
  static final String icEPath = "assets/icons/e-path.png";
  static final String icEPathCompound = "assets/icons/e-path-compound.png";
  static final String icEPhone = "assets/icons/e-phone.png";
  static final String icEReset = "assets/icons/e-reset.png";
  static final String icEViewed = "assets/icons/e-viewed.png";

  static final String icEmail = "assets/icons/email.png";
  static final String icEmailOn = "assets/icons/email_on.png";

  static final String icCamera = "assets/icons/ic_camera.png";
  static final String icCameraGreen = "assets/icons/ic_camera_green.png";

  static final String icBankMoney = "assets/icons/ic_bank_money.png";
  static final String icBankPackage = "assets/icons/ic_bank_package.png";
  static final String icBankPayment = "assets/icons/ic_bank_payment.png";
  static final String icBankSurface = "assets/icons/ic_bank_surface.png";

  static final String icHomeWhite = "assets/icons/home_white.png";
  static final String icHomeBlack = "assets/icons/home_black.png";
  static final String icHomeGrey = "assets/icons/home_grey.png";
  static final String icHomeRed = "assets/icons/home_red.png";

  static final String icFilterBlack = "assets/icons/filter_black.png";
  static final String icFilterWhite = "assets/icons/filter_white.png";

  static final String icGift = "assets/icons/gift.png";
  static final String icGiftP = "assets/icons/gift_p.png";
  static final String icGiftP2 = "assets/icons/gift_p2.png";

  static final String icApple = "assets/icons/apple.png";
  static final String icFacebook = "assets/icons/facebook.png";
  static final String icZalo = "assets/icons/zalo.png";
  static final String icGoogle = "assets/icons/google.png";
  static final String icGoogle2 = "assets/icons/google2.png";

  static final String icChat = "assets/icons/ic_chat.png";
  static final String icChatBubbles = "assets/icons/chat_bubbles.png";
  static final String icFacebookChat = "assets/icons/facebook_chat.png";

  static final String icBankCitibank = "assets/icons/bank_citibank.png";
  static final String icBankHsbc = "assets/icons/bank_hsbc.png";
  static final String icBankSacombank = "assets/icons/bank_sacombank.png";

  static final String icCard = "assets/icons/card.png";
  static final String icCardMaster = "assets/icons/card_master.png";
  static final String icCardVisa = "assets/icons/card_visa.png";

  static final String icAlarmOff = "assets/icons/alarm_off.png";
  static final String icAlarmOn = "assets/icons/alarm_on.png";
}
