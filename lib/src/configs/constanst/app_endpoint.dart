class AppEndpoint {
  AppEndpoint._();

  static const String BASE_URL = "https://kingbuy.vn";
  static const String BASE_IMAGE = "http://kingbuy.vn";

  static const int connectionTimeout = 15000;
  static const int receiveTimeout = 15000;
  static const String keyAuthorization = "Authorization";

  static const int SUCCESS = 200;
  static const int ERROR_TOKEN = 401;
  static const int ERROR_VALIDATE = 422;
  static const int ERROR_SERVER = 500;
  static const int ERROR_DISCONNECT = -1;

  // Đăng ký
  static const String REGISTER_NORMAL = '/api/registerApp';

  // Update account
  static const String ACCOUNT_UPDATE = "/api/updateInfoOfUser";

  // Đăng nhập
  static const String LOGIN_NORMAL = "/api/loginApp";
  static const String LOGIN_GOOGLE = "/api/loginAppGoogle";
  static const String LOGIN_APPLE = "/api/loginWithApple";
  static const String LOGIN_FB = "/api/loginAppSocial";

  // Quên mật khẩu - reset bằng email
  static const String RESET_PASSWORD_BY_EMAIL = "/resetPasswordByEmail";

  // Tin khuyến mãi, danh mục sản phẩm, danh sách ưu đãi cho bạn, tìm kiếm sản phẩm
  static const String GET_ALL_PROMOTION = "/api/getAllPromotion";
  static const String GET_ALL_CATEGORY = "/api/getAllCategories";
  static const String GET_MY_PROMOTION = "/api/getAllMyPromotion";
  static const String GET_SEARCH_PRODUCT = "/api/searchProduct";

  // Hiển thị tất cả sản phẩm theo danh mục, chi tiết và review sản phẩm, đánh giá sản phẩm
  static const String GET_PRODUCT_BY_CATEGORY = "/api/getProductsByCategory";
  static const String GET_PRODUCT_DETAIL = "/api/getSingleProduct";
  static const String GET_PRODUCT_REVIEW = "/api/getReviewByProduct";
  static const String GET_PRODUCT_RATING = "/api/ratingInfoByProduct";

  // Sản phẩm mới, sản phẩm bán chạy
  static const String GET_ALL_NEW_PRODUCT = "/api/getAllProductNew";
  static const String GET_SELLING_PRODUCT = "/api/getAllProductSelling";

  // Popup khi vào home
  static const String GET_POPUP_HOME = "/api/getPopup";

  // Contacts, Commitments, Configs
  static const String GET_CONTACT = "/api/contactUs";
  static const String GET_COMMITMENTS = "/api/getCommitments";
  static const String GET_CONFIGS = "/api/apiGetAppConfig";

  // Tỉnh/thành phố, quận/huyện
  static const String GET_PROVINCE = "/api/getAllProvince";
  static const String GET_DISTRICT = "/api/getDistrictByProvinceCode/03";
}
