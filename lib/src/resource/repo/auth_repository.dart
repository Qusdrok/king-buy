import 'dart:io';

import 'package:dio/dio.dart';

import '../../src.dart';

enum LoginType { facebook, google, normal }

class AuthRepository {
  AuthRepository._();

  static AuthRepository _instance;

  factory AuthRepository() {
    if (_instance == null) _instance = AuthRepository._();
    return _instance;
  }

  // Future<NetworkState<LoginResponse>> resetPasswordByEmail({String email}) async {
  //   bool isDisconnect = await WifiService.isDisconnect();
  //   if (isDisconnect) return NetworkState.withDisconnect();
  //   try {
  //     Map<String, dynamic> params = {
  //       "email": email,
  //     };
  //     Response response = await AppClients().post(AppEndpoint.RESET_PASSWORD_BY_EMAIL, data: params);
  //     print(response.data);
  //     return NetworkState(
  //       status: response.statusCode,
  //       response: NetworkResponse.fromJson(
  //         response.data,
  //         converter: (data) => LoginResponse.fromJson(data),
  //       ),
  //     );
  //   } on DioError catch (e) {
  //     return NetworkState.withError(e);
  //   }
  // }

  Future<NetworkState<DataModel>> register({
    String account,
    String password,
    String confirmPassword,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();
    try {
      Map<String, dynamic> params = {
        "identity": account,
        "password": password,
        "password_confirm": confirmPassword,
      };

      Response response = await AppClients().post(AppEndpoint.REGISTER_NORMAL, data: params);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => DataModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<DataModel>> login({
    String accessToken,
    LoginType loginType,
    String account,
    String password,
  }) async {
    Map<String, dynamic> params = {};
    String api;

    switch (loginType) {
      case LoginType.facebook:
        params.addAll({'access_token': accessToken});
        api = AppEndpoint.LOGIN_FB;
        break;
      case LoginType.google:
        params.addAll({'access_token': accessToken});
        api = AppEndpoint.LOGIN_GOOGLE;
        break;
      case LoginType.normal:
        params.addAll({
          'identity': account,
          'password': password,
        });
        api = AppEndpoint.LOGIN_NORMAL;
        break;
    }

    var response = await _sendRequestLogin(api, params);
    if (response.isSuccess && response.data?.token != null) await AppShared.setAccessToken(response.data.token);
    return response;
  }

  Future<NetworkState<DataModel>> _sendRequestLogin(String api, Map<String, dynamic> params) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().post(api, data: params);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => DataModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<DataModel>> accountUpdate({
    String token,
    String name,
    String phone_number,
    String dob,
    int gender,
    String email,
    File avatar,
    int is_change_password,
    String old_password,
    String new_password,
    String new_confirm_password,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        'authorization': 'Bearer ${token}',
        'name': name,
        'phone_number': phone_number,
        'date_of_birth': dob,
        'gender': gender,
        'email': email,
      };

      if (is_change_password == 1) {
        params.addAll({
          'is_change_password': is_change_password,
          'old_password': old_password,
          'new_password': new_password,
          'new_confirm_password': new_confirm_password,
        });
      }

      if (avatar != null) params.addAll({'avatar': (await MultipartFile.fromFile(avatar.path)).filename});
      Response response = await AppClients().post(AppEndpoint.ACCOUNT_UPDATE, data: params);

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => DataModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

//region Get Province, District
  Future<NetworkState<List<Province>>> getProvinces() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_PROVINCE);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => Province.listFromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<District>>> getDistricts() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_DISTRICT);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => District.listFromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

//endregion

//region Get Promotions, My Promotions, Categories
  Future<NetworkState<List<PromotionModel>>> getPromotions({
    int limit,
    int offset,
    String api,
    String title,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        'limit': limit ?? 3,
        'offset': offset,
      };

      Response response = await AppClients().get(
        api,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => PromotionModel.listFromJson(data[title]),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<CategoryModel>>> getCategories({int limit, int offset}) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        'limit': limit ?? 3,
        'offset': offset,
      };

      Response response = await AppClients().get(
        AppEndpoint.GET_ALL_CATEGORY,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => CategoryModel.listFromJson(data["categories"]),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getProducts({
    String api,
    String searchWord,
    int productId,
    int limit,
    int offset,
    int brandId,
    String title,
    PriceModel price,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        'limit': limit ?? 3,
        'offset': offset,
      };

      switch (api) {
        case AppEndpoint.GET_ALL_NEW_PRODUCT:
          params.addAll({'product_category_id': productId});
          break;
        case AppEndpoint.GET_PRODUCT_BY_CATEGORY:
          params.addAll({
            'product_category_id': productId,
            'searchWord': searchWord,
            'brand_id': brandId,
          });
          break;
        case AppEndpoint.GET_SEARCH_PRODUCT:
          params.addAll({
            'product_category_id': productId,
            'searchWord': searchWord,
            'brand_id': brandId,
            'price': {
              'from': price?.from ?? 1,
              'to': price?.to ?? 2222222222,
            },
          });
          break;
      }

      Response response = await AppClients().get(
        api,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ProductModel.listFromJson(title != null ? data[title] : data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ProductModel>>> getProductsByCategory({
    int productId,
    int limit,
    int offset,
    String keyword,
    String brandId,
  }) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {
        'product_category_id': productId,
        'searchWord': keyword,
        'brand_id': brandId,
        'limit': limit ?? 3,
        'offset': offset,
      };

      Response response = await AppClients().get(
        AppEndpoint.GET_PRODUCT_BY_CATEGORY,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ProductModel.listFromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<DataModel>> getProductDetail(int productId) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {'product_id': productId};
      Response response = await AppClients().get(
        AppEndpoint.GET_PRODUCT_DETAIL,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => DataModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<ReviewModel>>> getProductReviews(int productId) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {'product_id': productId};
      Response response = await AppClients().get(
        AppEndpoint.GET_PRODUCT_REVIEW,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ReviewModel.listFromJson(data['rows']),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<RatingModel>> getProductRatings(int productId) async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Map<String, dynamic> params = {'product_id': productId};
      Response response = await AppClients().get(
        AppEndpoint.GET_PRODUCT_RATING,
        queryParameters: params,
      );

      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => RatingModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<PopupModel>> getPopup() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_POPUP_HOME);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => PopupModel.fromJson(data),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<ContactsModel>> getContacts() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_CONTACT);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ContactsModel.fromJson(data[0]),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<ConfigsAppModel>> getConfigs() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_CONFIGS);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => ConfigsAppModel.fromJson(data["app_configs"]),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }

  Future<NetworkState<List<CommitmentModel>>> getCommitments() async {
    bool isDisconnect = await WifiService.isDisconnect();
    if (isDisconnect) return NetworkState.withDisconnect();

    try {
      Response response = await AppClients().get(AppEndpoint.GET_COMMITMENTS);
      return NetworkState(
        status: response.statusCode,
        response: NetworkResponse.fromJson(
          response.data,
          converter: (data) => CommitmentModel.listFromJson(data["rows"]),
        ),
      );
    } on DioError catch (e) {
      print("DioError: $e");
      return NetworkState.withError(e);
    }
  }
}
