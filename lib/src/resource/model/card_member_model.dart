import '../../src.dart';

class CardMemberModel {
  String member_card_number;
  int count;
  List<CouponModel> coupon_none_use;

  CardMemberModel(
    this.member_card_number,
    this.count,
    this.coupon_none_use,
  );

  factory CardMemberModel.fromJson(Map<String, dynamic> json) {
    return CardMemberModel(
      json['member_card_number'],
      json['coupon_none_use']['count'],
      CouponModel.listFromJson(
        json['coupon_none_use']['rows'],
      ),
    );
  }
}
