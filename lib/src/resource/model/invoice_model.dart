import 'package:intl/intl.dart';

import '../../src.dart';

class InvoiceModel {
  int id,
      user_id,
      payment_type,
      delivery_charge,
      total,
      discount,
      reward_points,
      status,
      deducted_points,
      payment,
      is_export_invoice,
      delivery_status,
      is_use_point,
      used_points,
      months_installment,
      prepay_installment_amount,
      prepay_installment,
      pay_monthly_installment;
  List<ProductInvoiceModel> items;
  String order_address,
      invoice_code,
      order_phone,
      order_phone_2,
      order_name,
      note,
      hour_time,
      date_time,
      updated_at,
      created_at,
      tax_code,
      company_address,
      company_name,
      company_email,
      delivery_date;

  InvoiceModel({
    this.id,
    this.user_id,
    this.payment_type,
    this.delivery_charge,
    this.total,
    this.discount,
    this.reward_points,
    this.status,
    this.order_address,
    this.order_phone,
    this.order_name,
    this.note,
    this.hour_time,
    this.date_time,
    this.updated_at,
    this.created_at,
    this.deducted_points,
    this.payment,
    this.is_export_invoice,
    this.items,
    this.order_phone_2,
    this.tax_code,
    this.company_address,
    this.company_name,
    this.company_email,
    this.delivery_date,
    this.invoice_code,
    this.delivery_status,
    this.is_use_point,
    this.used_points,
    this.months_installment,
    this.prepay_installment_amount,
    this.prepay_installment,
    this.pay_monthly_installment,
  });

  factory InvoiceModel.fromJson(Map<String, dynamic> json) {
    return InvoiceModel(
      id: json['id'],
      user_id: json['user_id'],
      payment_type: json['payment_type'],
      delivery_charge: json['delivery_charge'],
      total: json['total'],
      discount: json['discount'],
      reward_points: json['reward_points'],
      status: json['status'],
      order_address: json['order_address'],
      order_phone: json['order_phone'],
      order_name: json['order_name'],
      note: json['note'],
      hour_time: json['hour_time'],
      date_time: json['date_time'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      company_address: json['company_address'],
      deducted_points: json['deducted_points'],
      payment: json['payment'],
      is_export_invoice: json['is_export_invoice'],
      order_phone_2: json['order_phone_2'],
      tax_code: json['tax_code'],
      delivery_date: json['delivery_date'],
      invoice_code: json['invoice_code'],
      company_email: json['company_email'],
      company_name: json['company_name'],
      items: json['items'] != null ? ProductInvoiceModel.listFromJson(json['items']) : null,
      delivery_status: json['delivery_status'],
      is_use_point: json['is_use_point'],
      used_points: json['used_points'],
      months_installment: json['months_installment'],
      prepay_installment_amount: json['prepay_installment_amount'],
      prepay_installment: json['prepay_installment'],
      pay_monthly_installment: json['pay_monthly_installment'],
    );
  }

  static List<InvoiceModel> listFromJson(dynamic listJson) =>
      listJson != null ? List<InvoiceModel>.from(listJson.map((x) => InvoiceModel.fromJson(x))) : [];

  // them phuong thuc lay dinh dang chuoi ngay gio: 10:30 - Thứ 3,14/01/2020
  List<String> dateOfWeeks = ["", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy", "Chủ nhật"];

  String getStringCreateDate() {
    DateTime dt = DateFormat("hh:mm:ss yyyy-MM-dd").parse(this.created_at);
    if (dt != null)
      return DateFormat("hh:mm").format(dt) + " - " + dateOfWeeks[dt.weekday] + ", " + DateFormat("dd/MM/yyyy").format(dt);
    return "";
  }

  String getStringDeliveryDate() {
    DateTime dt = DateFormat("hh:mm:ss yyyy-MM-dd").parse(this.delivery_date);
    if (dt != null)
      return DateFormat("hh:mm").format(dt) + " - " + dateOfWeeks[dt.weekday] + ", " + DateFormat("dd/MM/yyyy").format(dt);
    return "";
  }
}

class InvoiceDetailModel {
  int product_id, price, qty, discount, total;

  InvoiceDetailModel({this.product_id, this.price, this.qty, this.discount, this.total});

  factory InvoiceDetailModel.fromJson(Map<String, dynamic> json) {
    return InvoiceDetailModel(
      product_id: json['product_id'],
      price: json['price'],
      qty: json['qty'],
      discount: json['discount'],
      total: json['total'],
    );
  }

  static List<InvoiceDetailModel> listFromJson(dynamic listJson) => listJson != null
      ? List<InvoiceDetailModel>.from(
          listJson.map((x) => InvoiceDetailModel.fromJson(x)),
        )
      : [];

  Map toJson() => {
        'product_id': product_id,
        'price': price,
        'qty': qty,
        'discount': discount,
        'total': total,
      };

  get getTotalPrice => this.price * this.qty;
}

class PaymentType {
  int payment_type;
  String name;

  String get payment_name {
    String tmp = "";
    switch (payment_type) {
      case 1:
        tmp = "Thanh toán bằng tiền mặt khi nhận hàng";
        break;
      case 2:
        tmp = "Chuyển khoản ngân hàng";
        break;
      case 3:
        tmp = "Thanh toán bằng thẻ Visa / Master Card";
        break;
      case 4:
        tmp = "Thanh toán trừ điểm trên thẻ";
        break;
      default:
        tmp = "Thanh toán bằng tiền mặt khi nhận hàng";
        break;
    }
    return tmp;
  }

  String get icon {
    switch (payment_type) {
      case 1:
        return AppImages.icBankPackage;
      case 2:
        return AppImages.icBankSurface;
      case 3:
        return AppImages.icBankPayment;
      case 4:
        return AppImages.icBankPayment;
      default:
        return AppImages.icBankPackage;
    }
  }

  PaymentType({this.payment_type, this.name});
}

class InstallmentDetailModel {
  int id,
      total,
      months_installment,
      prepay_installment_amount,
      prepay_installment,
      pay_monthly_installment,
      installment_type,
      bank_id;
  String installment_card_image, bank_image, bank_name;

  InstallmentDetailModel({
    this.id,
    this.total,
    this.months_installment,
    this.prepay_installment_amount,
    this.prepay_installment,
    this.pay_monthly_installment,
    this.installment_type,
    this.installment_card_image,
    this.bank_id,
    this.bank_name,
    this.bank_image,
  });

  factory InstallmentDetailModel.fromJson(Map<String, dynamic> json) {
    return InstallmentDetailModel(
      id: json['id'],
      total: json['total'],
      months_installment: json['months_installment'],
      prepay_installment_amount: json['prepay_installment_amount'],
      prepay_installment: json['prepay_installment'],
      pay_monthly_installment: json['pay_monthly_installment'],
      installment_type: json['installment_type'],
      installment_card_image: json['installment_card_image'],
      bank_id: json['bank_id'],
      bank_name: json['bank_name'],
      bank_image: json['bank_image'],
    );
  }
}
