class Province {
  String code;
  String name;

  Province({this.code, this.name});

  factory Province.fromJson(Map<String, dynamic> json) {
    return Province(
      code: json["code"],
      name: json["name"],
    );
  }

  static List<Province> listFromJson(dynamic listJson) => List<Province>.from(listJson.map((x) => Province.fromJson(x)));
}

class District {
  String code;
  String name_with_type;
  List<String> delivery_status;
  int ship_fee_bulky, ship_fee_not_bulky;

  District({
    this.code,
    this.name_with_type,
    this.delivery_status,
    this.ship_fee_bulky,
    this.ship_fee_not_bulky,
  });

  factory District.fromJson(Map<String, dynamic> json) {
    return District(
        code: json["code"],
        name_with_type: json["name_with_type"],
        delivery_status: json["delivery_status"] != null ? listStringFromJson(json["delivery_status"]) : [],
        ship_fee_bulky: json["ship_fee_bulky"],
        ship_fee_not_bulky: json["ship_fee_not_bulky"]);
  }

  static List<District> listFromJson(dynamic listJson) => List<District>.from(listJson.map((x) => District.fromJson(x)));

  static List<String> listStringFromJson(dynamic listJson) => List<String>.from(listJson.map((x) => x.toString()));
}

class Ward {
  String code;
  String name_with_type;

  Ward({this.code, this.name_with_type});

  factory Ward.fromJson(Map<String, dynamic> json) {
    return Ward(
      code: json["code"],
      name_with_type: json["name_with_type"],
    );
  }

  static List<Ward> listFromJson(dynamic listJson) => List<Ward>.from(listJson.map((x) => Ward.fromJson(x)));
}

class CreditModel {
  int id;
  String name, image_source;

  CreditModel({this.id, this.name, this.image_source});

  factory CreditModel.fromJson(Map<String, dynamic> json) {
    return CreditModel(
      id: json["id"],
      name: json["name"],
      image_source: json["image_source"],
    );
  }

  static List<CreditModel> listFromJson(dynamic listJson) =>
      List<CreditModel>.from(listJson.map((x) => CreditModel.fromJson(x)));
}

class BankModel {
  int id;
  String name, image_source;

  BankModel({this.id, this.name, this.image_source});

  factory BankModel.fromJson(Map<String, dynamic> json) {
    return BankModel(
      id: json["id"],
      name: json["name"],
      image_source: json["image_source"],
    );
  }

  static List<BankModel> listFromJson(dynamic listJson) =>
      List<BankModel>.from(listJson.map((x) => BankModel.fromJson(x)));
}

class InstallmentArguments {
  int product_id, installment_type, bank_id, installment_card_type, months_installment, prepay_installment;

  InstallmentArguments(
    product_id,
    installment_type,
    bank_id,
    installment_card_type,
    months_installment,
    prepay_installment,
  ) {
    this.product_id = product_id;
    this.installment_type = installment_type;
    this.bank_id = bank_id;
    this.installment_card_type = installment_card_type;
    this.months_installment = months_installment;
    this.prepay_installment = prepay_installment;
  }
}
