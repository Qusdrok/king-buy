import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ConfigsAppModel with ChangeNotifier {
  String fanpage, hot_line, privacy_and_terms, use_of_terms, ver_app, zalo;
  List<CommitmentModel> commitments;

  ConfigsAppModel({
    this.fanpage,
    this.hot_line,
    this.privacy_and_terms,
    this.use_of_terms,
    this.ver_app,
    this.commitments,
    this.zalo,
  });

  setData(ConfigsAppModel model) {
    this.fanpage = model.fanpage;
    this.hot_line = model.hot_line;
    this.ver_app = model.ver_app;
    this.privacy_and_terms = model.privacy_and_terms;
    this.use_of_terms = model.use_of_terms;
    this.commitments = model.commitments;
    this.zalo = model.zalo;
  }

  static ConfigsAppModel of(BuildContext context) {
    return Provider.of<ConfigsAppModel>(context, listen: false);
  }

  factory ConfigsAppModel.fromJson(Map<String, dynamic> json) {
    return ConfigsAppModel(
      fanpage: json['fanpage'],
      hot_line: json['hot_line'],
      privacy_and_terms: json['privacy_and_terms'],
      use_of_terms: json['use_of_terms'],
      ver_app: json['ver_app'],
      zalo: json['zalo'],
      commitments: CommitmentModel.listFromJson(json['commitments']),
    );
  }
}

class CommitmentModel {
  String short_title, title, description, created_at, updated_at;
  int display_order, id;

  CommitmentModel(
    this.short_title,
    this.title,
    this.description,
    this.created_at,
    this.updated_at,
    this.display_order,
    this.id,
  );

  factory CommitmentModel.fromJson(Map<String, dynamic> json) {
    return CommitmentModel(
      json['short_title'],
      json['title'],
      json['description'],
      json['created_at'],
      json['updated_at'],
      json['display_order'],
      json['id'],
    );
  }

  static List<CommitmentModel> listFromJson(dynamic listJson) =>
      listJson != null ? List<CommitmentModel>.from(listJson.map((x) => CommitmentModel.fromJson(x))) : [];
}
