class QuestionProductModel {
  String created_at, content;
  int id, user_id, product_id, status, type, is_like;
  List<AnswerProductModel> answers;

  QuestionProductModel({
    this.id,
    this.status,
    this.content,
    this.answers,
    this.created_at,
    this.is_like,
    this.product_id,
    this.type,
    this.user_id,
  });

  factory QuestionProductModel.fromJson(Map<String, dynamic> json) {
    return QuestionProductModel(
      id: json['id'],
      content: json['content'],
      answers: AnswerProductModel.listFromJson(json['answers']),
      created_at: json['create_at'],
      is_like: json['is_like'],
      product_id: json['product_id'],
      status: json['status'],
      type: json['type'],
      user_id: json['user_id'],
    );
  }

  setData(QuestionProductModel model) {
    this.id = model.id;
    this.type = model.type;
    this.product_id = model.product_id;
    this.answers = model.answers;
    this.is_like = model.is_like;
    this.status = model.status;
    this.created_at = model.created_at;
    this.user_id = model.user_id;
  }

  static List<QuestionProductModel> listFromJson(dynamic listJson) => listJson != null
      ? List<QuestionProductModel>.from(
          listJson.map((x) => QuestionProductModel.fromJson(x)),
        )
      : [];
}

class AnswerProductModel {
  String created_at, content, author_name;
  int id;

  AnswerProductModel(this.id, this.created_at, this.content, this.author_name);

  factory AnswerProductModel.fromJson(Map<String, dynamic> json) {
    return AnswerProductModel(
      json['id'],
      json['created_at'],
      json['content'],
      json['author_name'],
    );
  }

  setData(AnswerProductModel model) {
    this.id = model.id;
    this.author_name = model.author_name;
    this.content = model.content;
    this.created_at = model.created_at;
  }

  static List<AnswerProductModel> listFromJson(dynamic listJson) => listJson != null
      ? List<AnswerProductModel>.from(
          listJson.map((x) => AnswerProductModel.fromJson(x)),
        )
      : [];
}
