class RatingModel {
  int one_star_count, two_star_count, three_star_count, four_star_count, five_star_count, rating_count;
  double avg_rating;

  RatingModel(
    this.one_star_count,
    this.two_star_count,
    this.three_star_count,
    this.four_star_count,
    this.five_star_count,
    this.avg_rating,
    this.rating_count,
  );

  factory RatingModel.fromJson(Map<String, dynamic> json) {
    return RatingModel(
      json['one_star_count'],
      json['two_star_count'],
      json['three_star_count'],
      json['four_star_count'],
      json['five_star_count'],
      double.parse('${json['avg_rating']}'),
      json['rating_count'],
    );
  }
}

class ReviewModel {
  int id, user_id, product_id, status, star, is_buy;
  String name, phone_number, comment, created_at, updated_at, avatar_source;

  ReviewModel(
    this.id,
    this.user_id,
    this.product_id,
    this.status,
    this.star,
    this.name,
    this.phone_number,
    this.comment,
    this.created_at,
    this.updated_at,
    this.avatar_source,
    this.is_buy,
  );

  factory ReviewModel.fromJson(Map<String, dynamic> json) {
    return ReviewModel(
      json['id'],
      json['user_id'],
      json['product_id'],
      json['status'],
      json['star'],
      json['name'],
      json['phone_number'],
      json['comment'],
      json['created_at'],
      json['updated_at'],
      json['avatar_source'],
      json['is_buy'],
    );
  }

  static List<ReviewModel> listFromJson(dynamic json) => json != null
      ? List<ReviewModel>.from(
          json.map((x) => ReviewModel.fromJson(x)),
        )
      : [];
}
