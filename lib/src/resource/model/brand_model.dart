class BrandModel {
  String name, image_source, content, slug, created_at, updated_at;
  int id, display_order;

  BrandModel(
    this.name,
    this.image_source,
    this.content,
    this.slug,
    this.created_at,
    this.updated_at,
    this.id,
    this.display_order,
  );

  factory BrandModel.fromJson(Map<String, dynamic> json) {
    return BrandModel(
      json['name'],
      json['image_source'],
      json['content'],
      json['slug'],
      json['created_at'],
      json['updated_at'],
      json['id'],
      json['display_order'],
    );
  }

  static List<BrandModel> listFromJson(dynamic json) => json != null
      ? List<BrandModel>.from(
          json.map((x) => BrandModel.fromJson(x)),
        )
      : [];
}
