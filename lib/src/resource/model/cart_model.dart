import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../src.dart';

// giỏ hàng
class CartModel with ChangeNotifier {
  int id;
  int totalQuantily = 0; // tổng số sản phẩm
  List<CartItem> products = List<CartItem>(); // danh sách sản phẩm được thêm vào giỏ
  int delivery_address_id; // địa chỉ giao hàng được chọn
  String date_time = ""; // ngày nhận hàng dd/MM/yyyy
  String hour_time = ""; // giờ nhận hàng hh:mm-hh:mm
  int payment_type = 1; // phương thức giao hàng (1. COD, 2. Banking, 3. VISA, 4. Point Rewards)
  String note = ""; // Ghi chú đơn hàng
  int is_export_invoice = 0; // 1.Yêu cầu xuất hóa đơn VAT, 0.Không yêu cầu
  int coupon_id;
  int totalUnread;
  int delivery_status = 1;
  int save_point;

  CartModel({
    this.id,
    this.totalQuantily,
    this.products,
    this.delivery_address_id,
    this.date_time,
    this.hour_time,
    this.payment_type,
    this.note,
    this.is_export_invoice,
    this.coupon_id,
    this.totalUnread,
    this.delivery_status,
    this.save_point,
  });

  initCart() {
    this.totalQuantily = 0;
    this.products = new List<CartItem>();
    this.date_time = "";
    this.hour_time = "";
    this.note = "";
    this.is_export_invoice = 0;
    this.totalUnread = 0;
    this.delivery_status = 1;
    this.save_point = 0;
    return this;
  }

  static CartModel of(BuildContext context) {
    return Provider.of<CartModel>(context);
  }

  getCart() async {
    // return await CartProvider.instance.getCart();
  }

  // tính lại tổng số lượng sản phẩm của giỏ hàng
  void refreshQuantily() {
    //Tính tổng số lượng
    int q = 0;
    if (products != null && products.length > 0) {
      products.forEach((CartItem item) {
        q += item.qty;
      });
    }
    this.totalQuantily = q;

    // luu database
    updateCart();

    //Update state
    notifyListeners();
  }

  factory CartModel.fromJson(Map<String, dynamic> json) {
    return CartModel(
        totalQuantily: json['totalQuantily'],
        delivery_address_id: json['delivery_address_id'],
        date_time: json['date_time'],
        hour_time: json['hour_time'],
        payment_type: json['payment_type'],
        note: json['note'],
        is_export_invoice: json['is_export_invoice'],
        coupon_id: json['coupon_id'],
        totalUnread: json['totalUnread'],
        delivery_status: json['delivery_status'],
        save_point: json['save_point'],
        id: json['id']);
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "totalQuantily": totalQuantily,
      "delivery_address_id": delivery_address_id,
      "date_time": date_time,
      "hour_time": hour_time,
      "payment_type": payment_type,
      "note": note,
      "is_export_invoice": is_export_invoice,
      "coupon_id": coupon_id,
      "totalUnread": totalUnread,
      "delivery_status": delivery_status,
      "save_point": save_point
    };

    if (id != null) map['id'] = id;
    return map;
  }

  void setData(CartModel model) {
    // thiết lập các thông tin giỏ hàng đã chọn
    this.id = model.id;
    this.products = model.products;
    this.delivery_address_id = model.delivery_address_id;
    this.date_time = model.date_time;
    this.hour_time = model.hour_time;
    this.payment_type = model.payment_type;
    this.note = model.note;
    this.is_export_invoice = model.is_export_invoice;
    this.coupon_id = model.coupon_id;
    this.totalQuantily = model.totalQuantily;
    this.totalUnread = model.totalUnread;
    this.delivery_status = model.delivery_status;
    this.save_point = model.save_point;

    refreshQuantily();
  }

  setProducts(List<CartItem> items) {
    this.products = items;
    refreshQuantily();
  }

  // thêm sản phẩm vào giỏ hàng
  void addProduct(ProductModel productModel, {int color_id}) async {
    var product = (this.products != null && this.products.length > 0)
        ? this.products.firstWhere((val) => val.product_id == productModel.id, orElse: () => null)
        : null;
    if (product != null) {
      // san pham da co cap nhat so luong san pham vao sqlite
      CartItem cartItem; //CartProvider.instance.getCartItem("product_id", product.product_id);
      if (cartItem != null) {
        cartItem.qty = cartItem.qty + 1;
        cartItem.hasRead = 0;
        //await CartProvider.instance.updateCartItem(cartItem);
      } else {
        CartItem item =
            CartItem(qty: 1, product_id: productModel.id, productModel: productModel, hasRead: 0, color_id: color_id);
        //item.id = await CartProvider.instance.insertCartItem(item);
        this.products = this.products == null ? List<CartItem>() : this.products;
        this.products.add(item);
      }
      updateCartQuantity(product, true);
    } else {
      // san pham chua co tang so luon san pham len 1
      this.totalQuantily = this.totalQuantily == null ? 1 : this.totalQuantily + 1;

      CartItem item =
          CartItem(productModel: productModel, qty: 1, product_id: productModel.id, hasRead: 0, color_id: color_id);
      this.products = this.products == null ? List<CartItem>() : this.products;
      this.products.add(item);
      // them san pham vao sqlite
      //await CartProvider.instance.insertCartItem(item);
    }

    // tang so luong chua doc
    this.totalUnread = this.totalUnread == null ? 1 : this.totalUnread + 1;
    // luu cart
    updateCart();

    // cap nhat lai cart model
    notifyListeners();
  }

  // bớt sản phẩm ra khỏi giỏ
  void removeProduct(CartItem item) async {
    // xoa trong db sqlite truoc
    //await CartProvider.instance.deleteCartItem(item.id);

    // remove khoi cart item
    if (this.products != null && this.products.length > 0)
      this.products.removeWhere((val) => val.product_id == item.product_id);

    // giam so luong san pham
    this.totalQuantily = this.totalQuantily == null ? 0 : this.totalQuantily;
    if (this.totalQuantily > item.qty)
      this.totalQuantily = this.totalQuantily - item.qty;
    else
      this.totalQuantily = 0;

    // cap nhat lai cart model
    notifyListeners();
  }

  // cập nhật số lượng cart item (tăng/giảm)
  updateCartQuantity(CartItem item, bool isIncree) async {
    var product = (this.products != null && this.products.length > 0)
        ? this.products.firstWhere((val) => val.product_id == item.product_id, orElse: () => null)
        : null;
    if (product != null) {
      if (isIncree)
        product.plus();
      else
        product.minus();
      //await CartProvider.instance.updateCartItem(product);
    }
    this.totalQuantily = this.totalQuantily == null ? 0 : this.totalQuantily;
    this.totalQuantily += isIncree ? 1 : (this.totalQuantily > 0 ? -1 : 0);
    //save cart
    updateCart();
    notifyListeners();
  }

  // lấy tổng giá theo sản phẩm chưa tính hoa hồng
  int get totalPrice {
    int total = 0;
    if (products != null && products.length > 0) {
      products.forEach((CartItem model) {
        total += model.getTotalPrice;
      });
    }
    return total;
  }

  // chọn mã giảm giá
  setCoupon(int code) {
    this.coupon_id = code;
    notifyListeners();
  }

  // chọn địa chỉ nhận hàng
  setDeliveryAddressId(int id) {
    this.delivery_address_id = id;
    notifyListeners();

    // luu database
    updateCart();
  }

  // chọn ngày nhận hàng
  setDeliveryDate(String date_time) {
    this.date_time = date_time;
    notifyListeners();

    // luu database
    updateCart();
  }

  // chọn giờ nhận hàng
  setDeliveryHour(String hour_time) {
    this.hour_time = hour_time;
    notifyListeners();

    // luu database
    updateCart();
  }

  // chọn giờ nhận hàng
  setDeliveryStatus(int delivery_status) {
    this.delivery_status = delivery_status;
    notifyListeners();

    // luu database
    updateCart();
  }

  // chọn su dung tich diem
  setSavePoint(int save_point) {
    this.save_point = save_point;
    notifyListeners();

    // luu database
    updateCart();
  }

  // chọn xuất hoá đơn
  setExportInvoice(int is_export_invoice) {
    this.is_export_invoice = is_export_invoice;
    notifyListeners();
    updateCart();
  }

  // chọn phương thức thanh toán
  setPaymentMethod(int value) {
    this.payment_type = value;
    notifyListeners();

    // luu database
    updateCart();
  }

  // nhập ghi chú
  setNote(String value) {
    this.note = value;
    notifyListeners();

    // luu database
    updateCart();
  }

  updateCart() async {
    //await CartProvider.instance.updateCart(this);
  }

  deleteAllProducts() async {
    //await CartProvider.instance.deleteAllCartItem();
    this.products = List<CartItem>();
    this.totalUnread = 0;
    this.totalQuantily = 0;

    updateCart();
    notifyListeners();
  }

  readAll() async {
    if (this.products != null && this.products.length > 0) {
      this.products.map((item) => item.hasRead = 1);
      this.totalUnread = 0;
      notifyListeners();
      // update all row
      //await CartProvider.instance.readAll();
    } else {
      this.totalUnread = 0;
      notifyListeners();
    }
  }
}

class CartItem {
  int qty;

  // sqlite db
  int id;
  int product_id;
  ProductModel productModel;
  int hasRead;
  int color_id;

  CartItem({this.qty, this.productModel, this.id, this.product_id, this.hasRead, this.color_id});

  int get getTotalPrice {
    int p = 0;
    if (this.productModel != null) {
      if (this.productModel.sale_price != null && this.productModel.sale_price > 0)
        p = this.productModel.sale_price * this.qty;
      else
        p = this.productModel.price ?? 0 * this.qty;
    }
    return p != null ? p : 0;
  }

  plus() {
    qty = qty == null ? 0 : qty;
    qty++;
  }

  minus() {
    qty = qty == null ? 0 : qty;
    qty = qty > 0 ? (qty - 1) : 0;
  }

  // them phuong thuc de luu tru sql
  factory CartItem.fromJson(Map<String, dynamic> json) {
    return CartItem(
        id: json['id'],
        product_id: json['product_id'],
        color_id: json['color_id'],
        qty: json['qty'],
        hasRead: json['hasRead']);
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "product_id": product_id,
      "color_id": color_id,
      "qty": qty,
      "hasRead": hasRead,
    };
    if (id != null) {
      map['id'] = id;
    }

    return map;
  }

  static List<CartItem> listFromJson(dynamic listJson) =>
      listJson != null ? List<CartItem>.from(listJson.map((x) => CartItem.fromJson(x))) : [];

  // them phuong thuc de tao map luu csdl
  Map toJson() => {'product_id': product_id, 'color_id': color_id, 'qty': qty};
}

class DeliveryStatusType {
  int delivery_status;

  DeliveryStatusType({int delivery_status});

  String get delivery_status_name {
    String tmp = "";
    switch (delivery_status) {
      case 1:
        tmp = "Giao hàng trong 3h";
        break;
      case 2:
        tmp = "Giao hàng trong 24h";
        break;
      case 3:
        tmp = "Giao hàng 2-3 ngày";
        break;
      case 4:
        tmp = "Giao hàng 3-5 ngày";
        break;
      case 5:
        tmp = "Nhân viên gọi xác nhận";
        break;
      default:
        tmp = "Giao hàng trong 3h";
        break;
    }
    return tmp;
  }
}
