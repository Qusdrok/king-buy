class BannerModel {
  String title, image_source, description, content, slug, created_at, updated_at;
  int id, status;

  BannerModel({
    this.title,
    this.status,
    this.image_source,
    this.description,
    this.id,
    this.created_at,
    this.updated_at,
    this.slug,
    this.content,
  });

  factory BannerModel.fromJson(Map<String, dynamic> json) {
    return BannerModel(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      content: json['content'],
      status: json['status'],
      image_source: json['image_source'],
      created_at: json['created_at'],
    );
  }

  static List<BannerModel> listFromJson(dynamic json) => json != null
      ? List<BannerModel>.from(
          json.map((x) => BannerModel.fromJson(x)),
        )
      : [];
}
