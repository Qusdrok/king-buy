/// iserror : true
/// code : "Error_TokenInvalidate"
/// data : null

class NetworkResponse<T> {
  int status;
  String message;
  T data;

  NetworkResponse({
    this.status,
    this.data,
    this.message,
  });

  NetworkResponse.fromJson(dynamic json, {converter}) {
    status = json["status"];
    message = json["message"];
    data = converter != null
        ? json["data"] != null
            ? converter(json["data"])
            : json["data"]
        : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = status;
    map["message"] = message;
    map["data"] = data;
    return map;
  }
}
