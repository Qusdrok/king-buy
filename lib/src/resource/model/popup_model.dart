class PopupModel {
  PopupModel({
    this.popupImage,
    this.popupProductId,
    this.popupType,
    this.popupLink,
  });

  String popupImage;
  int popupProductId;
  int popupType;
  String popupLink;

  factory PopupModel.fromJson(Map<String, dynamic> json) => PopupModel(
        popupImage: json["popup_image"],
        popupProductId: json["popup_product_id"],
        popupType: json["popup_type"],
        popupLink: json["popup_link"],
      );

  Map<String, dynamic> toJson() => {
        "popup_image": popupImage,
        "popup_product_id": popupProductId,
        "popup_type": popupType,
        "popup_link": popupLink,
      };
}
