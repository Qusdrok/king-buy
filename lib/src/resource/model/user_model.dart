import 'package:flutter/material.dart';

class UserModel with ChangeNotifier {
  String name, email, phone_number, avatar_source, date_of_birth, created_at, updated_at;
  int gender, status, id, reward_points;

  UserModel({
    this.name,
    this.email,
    this.phone_number,
    this.avatar_source,
    this.date_of_birth,
    this.created_at,
    this.updated_at,
    this.gender,
    this.status,
    this.id,
    this.reward_points,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      name: json['name'],
      email: json['email'],
      avatar_source: json['avatar_source'],
      date_of_birth: json['date_of_birth'],
      phone_number: json['phone_number'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      gender: json['gender'],
      status: json['status'],
      reward_points: json['reward_points'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'phone_number': phone_number,
        'avatar_source': avatar_source,
        'date_of_birth': date_of_birth,
        'gender': 0,
        'status': 1,
        'created_at': created_at,
        'updated_at': updated_at,
        'reward_points': reward_points,
      };

  setRewardPoint(int point) => reward_points = point;

  static List<UserModel> listFromJson(dynamic json) => json != null
      ? List<UserModel>.from(
          json.map((x) => UserModel.fromJson(x)),
        )
      : [];
}
