class ParentModel {
  String name;
  int id;
  ParentModel parent;

  ParentModel(
    this.id,
    this.name,
    this.parent,
  );

  factory ParentModel.fromJson(Map<String, dynamic> json) {
    return json == null
        ? null
        : ParentModel(
            json['id'],
            json['name'],
            ParentModel.fromJson(json['parent']),
          );
  }

  static List<ParentModel> listFromJson(dynamic json) => json != null
      ? List<ParentModel>.from(
          json.map((x) => ParentModel.fromJson(x)),
        )
      : [];
}
