class ShopModel {
  String address, hot_line, opening_hours, image_source, latitude, longitude;
  int id;

  ShopModel(
    this.id,
    this.address,
    this.hot_line,
    this.opening_hours,
    this.image_source,
    this.latitude,
    this.longitude,
  );

  factory ShopModel.fromJson(Map<String, dynamic> json) {
    return ShopModel(
      json['id'],
      json['address'],
      json['hot_line'],
      json['opening_hours'],
      json['image_source'],
      json['latitude'],
      json['longitude'],
    );
  }

  setData(ShopModel model) {
    this.id = model.id;
    this.address = model.address;
    this.hot_line = model.hot_line;
    this.opening_hours = model.opening_hours;
    this.image_source = model.image_source;
    this.latitude = model.latitude;
    this.longitude = model.longitude;
  }

  static List<ShopModel> listFromJson(dynamic json) => json != null
      ? List<ShopModel>.from(
          json.map((x) => ShopModel.fromJson(x)),
        )
      : [];
}
