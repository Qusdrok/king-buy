import 'package:flutter/cupertino.dart';

/// "id":1,
///"name":"quà 1",
///"price":22222,
///"image_source":"/photos/1/IMG_3184 (1).jpg",
///"description":null,
///"created_at":"2020-02-14 14:35:56",
///"updated_at":"2020-02-14 14:35:56"

class GiftModel with ChangeNotifier {
  String name, image_source, description, created_at, updated_at;
  int id, price;

  GiftModel(
    this.name,
    this.image_source,
    this.description,
    this.created_at,
    this.updated_at,
    this.id,
    this.price,
  );

  factory GiftModel.fromJson(Map<String, dynamic> json) {
    return GiftModel(
      json['name'],
      json['image_source'],
      json['description'],
      json['created_at'],
      json['updated_at'],
      json['id'],
      json['price'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "name": name,
      "image_source": image_source,
      "description": description,
      "created_at": created_at,
      "updated_at": updated_at,
      "id": id,
      "price": price,
    };

    return map;
  }

  static List<GiftModel> listFromJson(dynamic json) => json != null
      ? List<GiftModel>.from(
          json.map((x) => GiftModel.fromJson(x)),
        )
      : [];
}
