import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../src.dart';

class FirebaseMassagingModel with ChangeNotifier {
  NotificationModel notificationData;

  FirebaseMassagingModel({this.notificationData});

  factory FirebaseMassagingModel.fromJson(Map<dynamic, dynamic> json) {
    return FirebaseMassagingModel(
      notificationData: NotificationModel(
        json['title'],
        json['description'],
        json['body'],
        null,
        null,
        null,
        null,
      ),
    );
  }

  @override
  String toString() {
    return '''{"title": "${this.notificationData.title}", "body": "${this.notificationData.content}", "description": "${this.notificationData.description}"}''';
  }

  setData(FirebaseMassagingModel model) {
    this.notificationData = model.notificationData;
  }

  static FirebaseMassagingModel of(BuildContext context) {
    return Provider.of<FirebaseMassagingModel>(context);
  }
}
