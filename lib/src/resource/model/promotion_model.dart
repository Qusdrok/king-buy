class PromotionModel {
  String title, image_source, description, content, slug, created_at, updated_at;
  int id, status;

  PromotionModel({
    this.title,
    this.status,
    this.image_source,
    this.description,
    this.id,
    this.created_at,
    this.updated_at,
    this.slug,
    this.content,
  });

  factory PromotionModel.fromJson(Map<String, dynamic> json) {
    return PromotionModel(
      id: json['id'],
      title: json['title'],
      status: json['status'],
      content: json['content'],
      description: json['description'],
      image_source: json['image_source'],
    );
  }

  static List<PromotionModel> listFromJson(dynamic json) => json != null
      ? List<PromotionModel>.from(
          json.map((x) => PromotionModel.fromJson(x)),
        )
      : [];
}
