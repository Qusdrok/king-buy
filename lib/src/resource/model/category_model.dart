import 'parent_model.dart';

class CategoryModel {
  String name, description, image_source, icon_source, background_image;
  int id;
  List<CategoryModel> children;
  ParentModel parent;

  CategoryModel({
    this.name,
    this.description,
    this.image_source,
    this.icon_source,
    this.id,
    this.background_image,
    this.children,
    this.parent,
  });

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      parent: ParentModel.fromJson(json['parent']),
      background_image: json['background_image'],
      children: CategoryModel.listFromJson(json['children']),
      icon_source: json['icon_source'],
      image_source: json['image_source'],
    );
  }

  static List<CategoryModel> listFromJson(dynamic json) => json != null
      ? List<CategoryModel>.from(
          json.map((x) => CategoryModel.fromJson(x)),
        )
      : [];
}
