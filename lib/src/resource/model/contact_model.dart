class ContactsModel {
  String message, hotLine, email;

  ContactsModel(
    this.message,
    this.hotLine,
    this.email,
  );

  factory ContactsModel.fromJson(Map<String, dynamic> json) {
    return ContactsModel(
      json['message'],
      json['hotLine'],
      json['email'],
    );
  }
}
