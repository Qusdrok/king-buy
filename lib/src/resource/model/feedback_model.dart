class MessageFeedbackModel {
  String body;
  int type;

  MessageFeedbackModel(
    this.type,
    this.body,
  );

  factory MessageFeedbackModel.fromJson(Map<String, dynamic> json) {
    return MessageFeedbackModel(
      json['type'],
      json['body'],
    );
  }

  static List<MessageFeedbackModel> listFromJson(dynamic json) => json != null
      ? List<MessageFeedbackModel>.from(
          json.map((x) => MessageFeedbackModel.fromJson(x)),
        )
      : [];
}

class FeedbackModel {
  String title, bodyFirst;
  List feedback;

  FeedbackModel(
    this.title,
    this.bodyFirst,
    this.feedback,
  );

  factory FeedbackModel.fromJson(Map<String, dynamic> json) {
    return FeedbackModel(
      json['titleFirst'],
      json['contentFirst'],
      json['feedback'],
    );
  }
}
