import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DeliveryLocationModel {
  int id, user_id, is_default, is_export_invoice, money_ship;
  String address,
      province_code,
      ward_code,
      district_code,
      company_name,
      tax_code,
      company_address,
      company_email,
      first_phone,
      second_phone,
      full_name, // kiem tra lai fullname
      distance,
      full_address;
  List<String> delivery_status;
  int ship_fee_bulky, ship_fee_not_bulky;
  String localFullAddress;

  DeliveryLocationModel({
    this.id,
    this.user_id,
    this.is_default,
    this.is_export_invoice,
    this.money_ship,
    this.address,
    this.province_code,
    this.ward_code,
    this.district_code,
    this.company_name,
    this.tax_code,
    this.company_address,
    this.company_email,
    this.first_phone,
    this.second_phone,
    this.full_name,
    this.distance,
    this.full_address,
    this.delivery_status,
    this.ship_fee_bulky,
    this.ship_fee_not_bulky,
    this.localFullAddress,
  });

  void setData(DeliveryLocationModel addressModel) {
    this.id = addressModel.id;
    this.user_id = addressModel.user_id;
    this.is_default = addressModel.is_default;
    this.is_export_invoice = addressModel.is_export_invoice;
    this.money_ship = addressModel.money_ship;
    this.address = addressModel.address;
    this.province_code = addressModel.province_code;
    this.ward_code = addressModel.ward_code;
    this.district_code = addressModel.district_code;
    this.company_name = addressModel.company_name;
    this.tax_code = addressModel.tax_code;
    this.company_address = addressModel.company_address;
    this.company_email = addressModel.company_email;
    this.first_phone = addressModel.first_phone;
    this.second_phone = addressModel.second_phone;
    this.full_name = addressModel.full_name;
    this.distance = addressModel.distance;
    this.full_address = addressModel.full_address;
    this.delivery_status = addressModel.delivery_status;
    this.ship_fee_bulky = addressModel.ship_fee_bulky;
    this.ship_fee_not_bulky = addressModel.ship_fee_not_bulky;
    this.localFullAddress = addressModel.localFullAddress;
  }

  factory DeliveryLocationModel.fromJson(Map<String, dynamic> json) {
    return DeliveryLocationModel(
      id: json['id'],
      user_id: json['user_id'],
      is_default: json['is_default'],
      is_export_invoice:
          json['is_export_invoice'] is String ? int.parse(json['is_export_invoice']) : json['is_export_invoice'],
      money_ship: json['money_ship'],
      address: json['address'],
      province_code: json['province_code'],
      ward_code: json['ward_code'],
      district_code: json['district_code'],
      company_name: json['company_name'],
      tax_code: json['tax_code'],
      company_address: json['company_address'],
      company_email: json['company_email'],
      first_phone: json['first_phone'],
      second_phone: json['second_phone'],
      full_name: json['fullname'],
      distance: json['distance'],
      full_address: json['full_address'],
      delivery_status:
          json['delivery_status'] != null ? List<String>.from(json['delivery_status'].map((x) => x.toString())) : [],
      ship_fee_bulky: json['ship_fee_bulky'],
      ship_fee_not_bulky: json['ship_fee_not_bulky'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      "id": id,
      "user_id": user_id,
      "is_default": is_default,
      "is_export_invoice": is_export_invoice,
      "money_ship": money_ship,
      "address": address,
      "province_code": province_code,
      "ward_code": ward_code,
      "district_code": district_code,
      "company_name": company_name,
      "tax_code": tax_code,
      "company_address": company_address,
      "company_email": company_email,
      "first_phone": first_phone,
      "second_phone": second_phone,
      "fullname": full_name,
      "distance": distance,
      "full_address": full_address,
      "delivery_status": delivery_status,
      "ship_fee_bulky": ship_fee_bulky,
      "ship_fee_not_bulky": ship_fee_not_bulky,
    };

    return map;
  }

  String toString() {
    return "(" +
        "'$id','$user_id','$is_default','$is_export_invoice','$money_ship','$address','$province_code'"
            ",'$ward_code','$district_code','$company_name','$tax_code','$company_address','$company_email'" +
        ",'$first_phone','$second_phone','$full_name','$distance','$full_address','$delivery_status','$ship_fee_bulky','$ship_fee_not_bulky'" +
        ")";
  }

  static List<DeliveryLocationModel> listFromJson(dynamic json) =>
      json != null ? List<DeliveryLocationModel>.from(json.map((x) => DeliveryLocationModel.fromJson(x))) : [];

  setLocalFullAddress(String address, String ward, String district, String province) {
    localFullAddress = address + ", " + ward + ", " + district + ", " + province;
  }
}

class ListAddressModel with ChangeNotifier {
  List<DeliveryLocationModel> listAddress = List<DeliveryLocationModel>();

  static ListAddressModel of(BuildContext context) {
    return Provider.of<ListAddressModel>(context);
  }

  void setData(List<DeliveryLocationModel> addresss) {
    if (this.listAddress != null && this.listAddress.length > 0) this.listAddress.clear();
    this.listAddress = addresss;
    notifyListeners();
  }

  DeliveryLocationModel getDefaultAddress() {
    if (listAddress != null && listAddress.length > 0) {
      DeliveryLocationModel addressModel = listAddress.firstWhere((model) => model.is_default == 1, orElse: () => null);
      return addressModel;
    }
    return null;
  }

  DeliveryLocationModel getAddressById(int id) {
    if (listAddress != null && listAddress.length > 0) {
      DeliveryLocationModel addressModel = listAddress.firstWhere((model) => model.id == id, orElse: () => null);
      return addressModel;
    }
    return null;
  }

  void updateLocalAddress(DeliveryLocationModel address) {
    if (listAddress != null && listAddress.length > 0) listAddress.clear();
    listAddress.add(address);
  }
}
