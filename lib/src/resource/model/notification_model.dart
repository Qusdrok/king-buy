import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class NotificationModel {
  String title, description, content, slug, created_at, updated_at;
  int status;

  NotificationModel(
    this.title,
    this.description,
    this.content,
    this.slug,
    this.status,
    this.created_at,
    this.updated_at,
  );

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      json['title'],
      json['description'],
      json['content'],
      json['slug'],
      json['status'],
      json['created_at'],
      json['updated_at'],
    );
  }

  static List<NotificationModel> listFromJson(dynamic listJson) => listJson != null
      ? List<NotificationModel>.from(listJson.map(
          (x) => NotificationModel.fromJson(x),
        ))
      : [];
}

class NotificationDataModel with ChangeNotifier {
  int numberUnread;

  static NotificationDataModel of(BuildContext context) {
    return Provider.of<NotificationDataModel>(context);
  }

  NotificationDataModel({this.numberUnread});

  setData(NotificationDataModel data) {
    this.numberUnread = data.numberUnread;
  }

  get unread => numberUnread;

  increment() => numberUnread++;

  abatement() => numberUnread--;
}
