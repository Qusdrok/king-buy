import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CouponAllModel with ChangeNotifier {
  List<CouponModel> couponNonUse, couponUsed, couponExpired;

  CouponAllModel({
    this.couponNonUse,
    this.couponUsed,
    this.couponExpired,
  });

  setData(CouponAllModel model) {
    this.couponNonUse = model.couponNonUse;
    this.couponUsed = model.couponUsed;
    this.couponExpired = model.couponExpired;
  }

  static CouponAllModel of(BuildContext context) {
    return Provider.of<CouponAllModel>(context, listen: true);
  }

  factory CouponAllModel.fromJson(Map<String, dynamic> json) {
    return CouponAllModel(
      couponNonUse: CouponModel.listFromJson(json['non_use']['rows']),
      couponUsed: CouponModel.listFromJson(json['used']['rows']),
      couponExpired: CouponModel.listFromJson(json['expired']['rows']),
    );
  }
}

class CouponModel {
  String name, description, image_source, expires_at;
  int id, apply_target, sale_price, percent_off, invoice_total, type, invoice_status, max_sale, status, product_id;

  CouponModel({
    this.name,
    this.description,
    this.image_source,
    this.expires_at,
    this.invoice_status,
    this.max_sale,
    this.status,
    this.apply_target,
    this.type,
    this.id,
    this.sale_price,
    this.percent_off,
    this.invoice_total,
    this.product_id,
  });

  factory CouponModel.fromJsonForList(Map<String, dynamic> json) {
    return CouponModel(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      image_source: json['image_source'],
      expires_at: json['expires_at'],
    );
  }

  factory CouponModel.fromJson(Map<String, dynamic> json) {
    return CouponModel(
      status: json['status'],
      id: json['id'],
      invoice_status: json['invoice_status'],
      max_sale: json['max_sale'],
      name: json['name'],
      description: json['description'],
      image_source: json['image_source'],
      expires_at: json['expires_at'],
      sale_price: json['sale_price'],
      apply_target: json['apply_target'],
      invoice_total: json['invoice_total'],
      percent_off: json['percent_off'],
      type: json['type'],
      product_id: json['product_id'],
    );
  }

  static List<CouponModel> listFromJson(dynamic listJson) => listJson != null
      ? List<CouponModel>.from(
          listJson.map((x) => CouponModel.fromJsonForList(x)),
        )
      : [];
}

class CouponCheckData {
  int type, discount;

  CouponCheckData({
    this.type,
    this.discount,
  });

  factory CouponCheckData.fromJson(Map<String, dynamic> json) {
    return CouponCheckData(
      type: json['type'],
      discount: json['discount'],
    );
  }
}
