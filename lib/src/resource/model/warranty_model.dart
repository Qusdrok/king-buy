class WarrantyModel {
  String status_of_receipt,
      date_purchase,
      date_received,
      comment,
      wr_type_name,
      total_amount_fc2,
      product_code,
      product_name,
      status_id,
      status_name,
      status;

  WarrantyModel(
    this.status,
    this.status_of_receipt,
    this.date_purchase,
    this.date_received,
    this.comment,
    this.wr_type_name,
    this.total_amount_fc2,
    this.product_code,
    this.product_name,
    this.status_id,
    this.status_name,
  );

  factory WarrantyModel.fromJson(Map<String, dynamic> json) {
    return WarrantyModel(
      json['status'],
      json['status_of_receipt'],
      json['date_purchase'],
      json['date_received'],
      json['comment'],
      json['wr_type_name'],
      json['total_amount_fc2'],
      json['product_code'],
      json['product_name'],
      json['status_id'],
      json['status_name'],
    );
  }

  setData(WarrantyModel model) {
    this.status = model.status;
    this.status_of_receipt = model.status_of_receipt;
    this.date_purchase = model.date_purchase;
    this.date_received = model.date_received;
    this.comment = model.comment;
    this.wr_type_name = model.wr_type_name;
    this.total_amount_fc2 = model.total_amount_fc2;
    this.product_code = model.product_code;
    this.product_name = model.product_name;
    this.status_id = model.status_id;
    this.status_name = model.status_name;
  }

  static List<WarrantyModel> listFromJson(dynamic json) => json != null
      ? List<WarrantyModel>.from(
          json.map((x) => WarrantyModel.fromJson(x)),
        )
      : [];
}
