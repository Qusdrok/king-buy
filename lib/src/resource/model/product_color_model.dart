class ProductColorModel {
  String name;
  int id;
  List<String> image_source;

  ProductColorModel(
    this.id,
    this.image_source,
    this.name,
  );

  factory ProductColorModel.fromJson(Map<String, dynamic> json) {
    // print("______COLORS______");
    // print(json);
    return ProductColorModel(
      json['id'],
      List<String>.from(json['image_source'].map((x) => x)),
      json['name'],
    );
  }

  setData(ProductColorModel model) {
    this.id = model.id;
    this.name = model.name;
    this.image_source = model.image_source;
  }

  static List<ProductColorModel> listFromJson(dynamic json) => json != null
      ? List<ProductColorModel>.from(
          json.map((x) => ProductColorModel.fromJson(x)),
        )
      : [];
}
