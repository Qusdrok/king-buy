import '../../src.dart';

class DataModel {
  DataModel({
    this.product,
    this.shopAddress,
    this.review,
    this.reviews,
    this.token,
    this.member_card_number,
    this.reward_points,
    this.user,
  });

  ProductModel product;
  List<ShopModel> shopAddress;
  ReviewModel review;
  List<ReviewModel> reviews;
  String token;
  String member_card_number;
  int reward_points;
  UserModel user;
  RatingModel rating;

  factory DataModel.fromJson(Map<String, dynamic> json) => DataModel(
        product: json["product"] != null ? ProductModel.fromJson(json["product"]) : null,
        shopAddress: json["shop_address"] != null
            ? List<ShopModel>.from(json["shop_address"].map((x) => ShopModel.fromJson(x)))
            : null,
        review: json["rating"] != null ? ReviewModel.fromJson(json["rating"]) : null,
        token: json['token'],
        reward_points: json['reward_points'],
        member_card_number: json['member_card_number'],
        user: json['profile'] != null ? UserModel.fromJson(json['profile']) : null,
      );
}
