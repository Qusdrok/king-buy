import '../../src.dart';

class DataFilter {
  static reset() {
    DataFilter.popup = null;
    DataFilter.price = null;
    DataFilter.brand = null;
    DataFilter.category = null;
    DataFilter.config = null;
    DataFilter.contacts = null;
    return;
  }

  static List<PriceModel> prices = [
    PriceModel(0, 8, 10),
    PriceModel(1, 10, 15),
    PriceModel(2, 15, 20),
    PriceModel(3, 20, 25),
    PriceModel(4, 25, 30),
    PriceModel(5, 30, 40),
    PriceModel(6, 40, 50),
    PriceModel(7, 50, 100),
    PriceModel(8, 100, 200),
    PriceModel(9, 200, 0),
  ];

  static BrandModel brand;
  static CategoryModel category;
  static PriceModel price;
  static PopupModel popup;
  static ContactsModel contacts;
  static ConfigsAppModel config;
  static List<CategoryModel> categories;
}

class PriceModel {
  int id, from, to;

  PriceModel(this.id, this.from, this.to);
}
