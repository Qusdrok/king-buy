import '../../src.dart';

class ProductModel {
  String name,
      image_source,
      description,
      content,
      slug,
      barcode,
      product_category_name,
      brand_name,
      brand_info,
      created_at,
      updated_at,
      video_link,
      specifications;

  int id, price, sale_price, product_category_id, brand_id, sale_off, status;
  int goods_status; // trang thai hang: 1. Còn hàng, 2. Hết hàng, 3. Ngừng kinh doanh
  int is_bulky, is_installment;
  double star;

  List<ProductColorModel> colors;
  List<int> gift_ids;
  List<GiftModel> gifts;
  List<dynamic> image_source_list;
  ParentModel category;

  ProductModel({
    this.colors,
    this.name,
    this.image_source,
    this.status,
    this.description,
    this.content,
    this.specifications,
    this.slug,
    this.barcode,
    this.product_category_name,
    this.brand_name,
    this.brand_info,
    this.created_at,
    this.updated_at,
    this.price,
    this.sale_price,
    this.product_category_id,
    this.brand_id,
    this.sale_off,
    this.star,
    this.gift_ids,
    this.id,
    this.gifts,
    this.video_link,
    this.goods_status,
    this.is_bulky,
    this.is_installment,
    this.category,
    this.image_source_list,
  });

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      id: json['id'],
      name: json['name'],
      specifications: json['specifications'],
      image_source: json['image_source'],
      barcode: json['barcode'],
      status: json['status'],
      image_source_list: json['image_source_list'] as List ?? [],
      description: json['description'],
      content: json['content'],
      brand_info: json['brand_info'],
      price: json['price'],
      slug: json['slug'],
      product_category_name: json['product_category_name'],
      brand_name: json['brand_name'],
      sale_price: json['sale_price'],
      created_at: json['created_at'],
      updated_at: json['updated_at'],
      product_category_id: json['product_category_id'],
      sale_off: json['sale_off'],
      colors: ProductColorModel.listFromJson(json['colors']),
      star: double.parse('${json['star']}'),
      brand_id: json['brand_id'],
      gifts: GiftModel.listFromJson(json['gifts']),
      video_link: json['video_link'],
      goods_status: json['goods_status'],
      is_bulky: json['is_bulky'],
      is_installment: json['is_installment'],
      category: ParentModel.fromJson(json['category']),
    );
  }

  static List<ProductModel> listFromJson(dynamic listJson) => listJson != null
      ? List<ProductModel>.from(
          listJson.map((x) => ProductModel.fromJson(x)),
        )
      : [];
}

class ProductInvoiceModel {
  int id, product_id, invoice_id, price, qty, total;
  String image_source, product_name, created_at, updated_at, color_name;
  List<GiftModel> gifts;

  ProductInvoiceModel(this.id, this.product_id, this.invoice_id, this.price, this.qty, this.total, this.image_source,
      this.product_name, this.created_at, this.updated_at, this.color_name, this.gifts);

  factory ProductInvoiceModel.fromJson(Map<String, dynamic> json) {
    return ProductInvoiceModel(
      json['id'],
      json['product_id'],
      json['invoice_id'],
      json['price'],
      json['qty'],
      json['total'],
      json['image_source'],
      json['product_name'],
      json['created_at'],
      json['updated_at'],
      json['color_name'],
      GiftModel.listFromJson(json['gifts']),
    );
  }

  static List<ProductInvoiceModel> listFromJson(dynamic listJson) => listJson != null
      ? List<ProductInvoiceModel>.from(
          listJson.map((x) => ProductInvoiceModel.fromJson(x)),
        )
      : [];
}
